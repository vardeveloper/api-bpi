import os

import sentry_sdk
from dotenv import load_dotenv
from fastapi import FastAPI
from mangum import Mangum
from sentry_sdk.integrations.aws_lambda import AwsLambdaIntegration

from api.routers import (annulations, billing, mapcity, peruquiosco, siebel,
                         subscribers, subscriptions)

# Carga variables de entorno de .env
load_dotenv()


# Integración con Sentry
sentry_sdk.init(
    os.getenv('SENTRY_DNS', ''),
    integrations=[AwsLambdaIntegration(), ],
    environment=os.getenv('ENVIRONMENT', '')
)


APP_PREFIX = "/api/v1.1"
app = FastAPI(
    title="Autogestión de callcenter",
    description="Para realizar las consultas el cliente debe enviar el header `Authorization: Bearer <token>`.",
    version="1.1",
)
app.include_router(subscribers.router, prefix=APP_PREFIX)
app.include_router(subscriptions.router, prefix=APP_PREFIX)
app.include_router(peruquiosco.router, prefix=APP_PREFIX)
app.include_router(siebel.router, prefix=APP_PREFIX)
app.include_router(annulations.router, prefix=APP_PREFIX)
app.include_router(billing.router, prefix=APP_PREFIX)
app.include_router(mapcity.router, prefix=APP_PREFIX)

handler = Mangum(app)
