from flask import abort
from utils import date_to_string

from .base import AutogestionDetailView, AutogestionListView, SiebelBaseMixin


class SubscriptionBundleMixin(SiebelBaseMixin):

    def get_connection(self):
        return self.get_subsonline_connection()

    def get_status(self, subscription):
        status = ''
        code = subscription['status']
        if code == 0:
            status = 'Inactivo'
        elif code == 1:
            status = 'Activo'
        elif code == 2:
            status = 'Anulado'
        elif code == 3:
            status = 'Declinado'
        elif code == 4:
            status = 'Cancelacion Solicitada'
        elif code == 5:
            status = 'Error'
        return status

    def get_portal(self, subscription):
        portal = subscription['portal']
        portal = 'elcomercio' if portal == 'comercio' else portal
        return portal


class SubscriptionBundleListView(SubscriptionBundleMixin, AutogestionListView):

    def get_query(self):
        query = '''
            SELECT
                "payment_psuscripcion"."id" as subscription_id,
                "payment_psuscripcion"."estado" as status,
                "payment_psuscripcion"."siebel_delivery" as delivery,
                "pgp_tipo_suscripcion"."tips_nombre" as periodicity,
                "epa_producto"."prod_nombre" as product,
                "sis_partners"."partner_codigo" as portal
            FROM
                "payment_psuscripcion"
                INNER JOIN "pgp_perfil_pago"
                        ON ( "payment_psuscripcion"."perfil_pago_id" =
                                "pgp_perfil_pago"."id" )
                INNER JOIN "sis_partners"
                        ON ( "payment_psuscripcion"."partner_id" = "sis_partners"."id" )
                INNER JOIN "payment_plan"
                        ON ( "payment_psuscripcion"."plan_id" = "payment_plan"."id" )
                INNER JOIN "pgp_tipo_suscripcion"
                        ON ( "payment_plan"."tips_id" = "pgp_tipo_suscripcion"."id" )
                INNER JOIN "epa_producto"
                        ON ( "pgp_tipo_suscripcion"."prod_id" = "epa_producto"."id" )
            WHERE  ( "pgp_perfil_pago"."ppago_tipodoc" = %(document_type)s
                    AND LOWER("pgp_perfil_pago"."ppago_numdoc") = LOWER(%(document_number)s)
        '''

        if self.clean_portal():
            query += ''' AND "sis_partners"."partner_codigo" = %(portal)s '''

        query += '''
                    AND "epa_producto"."is_digital" = true)
            ORDER  BY "payment_psuscripcion"."id" DESC
            LIMIT 100;
        '''
        return query

    def get(self):
        """
        This function fetches content from MySQL RDS instance
        """
        subscriptions = self.get_objects()

        delivery_list = []
        subscriptions_data = []
        for subscription in subscriptions:
            delivery = str(subscription['delivery'] or '')
            subscriptions_data.append(
                {
                    'category': 'bundle',
                    'id': str(subscription['subscription_id']),
                    'delivery': delivery,
                    'periodicity': subscription['periodicity'],
                    'portal': self.get_portal(subscription),
                    'product': subscription['product'],
                    'status': self.get_status(subscription),
                }
            )
            delivery_list.append(delivery)

        siebel_subscriptions = self.get_bundle_subscriptions()

        for subscription in siebel_subscriptions:
            delivery = str(subscription['CodDelivery'] or '')
            if delivery not in delivery_list:
                subscription_data = self.get_subscription_data(delivery)

                status = subscription_data.get('Estado').title()
                siebel_number = subscription_data.get('NroSiebel') or ''

                subscriptions_data.append(
                    {
                        'category': 'bundle',
                        'id': delivery,
                        'delivery': delivery,
                        'periodicity': subscription['Periodo'].title(),
                        'portal': self.get_subscription_portal(subscription),
                        'product': subscription['DescPaquete'],
                        'siebelNumber': siebel_number,
                        'status': status,
                    }
                )

        return self.render(
            {'subscriptions': subscriptions_data}
        )

    def get_query_kwargs(self):
        query_kwargs = super().get_query_kwargs()

        # Cambia elcomercio por comercio
        portal = self.clean_portal()
        query_kwargs['portal'] = 'comercio' if portal == 'elcomercio' else portal

        return query_kwargs


class SubscriptionBundleDetailView(SubscriptionBundleMixin, AutogestionDetailView):

    def get_query(self):
        query = '''
            SELECT
                "payment_psuscripcion"."id" as subscription_id,
                "payment_psuscripcion"."estado" as status,
                "payment_psuscripcion"."siebel_delivery" as delivery,
                "payment_psuscripcion"."fecha_cargo" as next_payment_date,
                "pgp_tipo_suscripcion"."tips_nombre" as periodicity,
                "pgp_tipo_suscripcion"."tips_precio" as price,
                "epa_producto"."prod_nombre" as product,
                "epa_producto_detalle"."pdet_dias_semana" as days
            FROM
                "payment_psuscripcion"
                INNER JOIN "pgp_perfil_pago"
                        ON ( "payment_psuscripcion"."perfil_pago_id" =
                                "pgp_perfil_pago"."id" )
                INNER JOIN "sis_partners"
                        ON ( "payment_psuscripcion"."partner_id" = "sis_partners"."id" )
                LEFT OUTER JOIN "payment_plan"
                                ON ( "payment_psuscripcion"."plan_id" =
                                "payment_plan"."id" )
                LEFT OUTER JOIN "pgp_tipo_suscripcion"
                                ON ( "payment_plan"."tips_id" =
                                "pgp_tipo_suscripcion"."id" )
                LEFT OUTER JOIN "epa_producto"
                                ON ( "pgp_tipo_suscripcion"."prod_id" =
                                "epa_producto"."id" )
                LEFT OUTER JOIN "epa_producto_detalle"
                                ON ( "epa_producto_detalle"."prod_id" =
                                "epa_producto"."id" )
            WHERE
                "payment_psuscripcion"."id" = %(subscription_id)s
            LIMIT 1;
        '''
        return query

    def get(self, subscription_id):
        """
        This function fetches content from MySQL RDS instance
        """
        try:
            subscription = self.get_object(
                subscription_id=subscription_id
            )
        except:
            subscription = None

        if not subscription:
            subscription = self.get_subscription_data(
                subscription_id=subscription_id
            )

            if not subscription:
                abort(404, description="Suscripción no encontrada")

            subscriber, _ = self.get_subscriber_data(
                document_type=subscription['TipDocumento'],
                document_number=subscription['NroDocumento']
            )

            # XXX "Portal": "EL COMERCIO"
            data = {
                "address": subscription.get('DirecReparto') or '',
                "category": subscription['Tipo'].lower(),
                "delivery": subscription_id,
                "deliveryDays": subscription.get('DiasReparto') or '',
                "district": subscription.get('DistReparto') or '',
                "email": subscriber.get('Email') or '',
                "id": subscription_id,
                "lastPaymentAmount": str(subscription['MntUltPago']),
                "lastPaymentDate": subscription['FchUltPago'],
                "name": subscription['DescPaquete'],
                "nextPaymentAmount": "",  # XXX Mock
                "nextPaymentDate": subscription['FchProxPago'],
                "periodicity": subscription['Periodo'].title(),
                "price": str(subscription['PrecioActual']),
                "siebelNumber": subscription['NroSiebel'] or '',
                "status": subscription['Estado'].title(),
            }
        else:
            last_payment = self.get_last_payment(
                subscription_id=subscription_id
            )

            data = {
                "category": "bundle",
                "deliveryDays": subscription["days"],
                "delivery": str(subscription["delivery"] or ''),
                "email": "",  # XXX Mock
                "id": str(subscription["subscription_id"]),
                "lastPaymentAmount": self.get_payment_amount(last_payment),
                "lastPaymentDate": self.get_payment_date(last_payment),
                "name": subscription['product'],
                "nextPaymentAmount": self.get_next_payment_amount(subscription),
                "nextPaymentDate": self.get_next_payment_date(subscription),
                "periodicity": subscription['periodicity'],
                "price": self.get_next_payment_amount(subscription),
                "status": self.get_status(subscription),
                # XXX address
                # XXX district
            }

        return self.render(data)

    def get_payment_date(self, payment):
        payment_date = payment['payment_date']
        return date_to_string(payment_date)

    def get_payment_amount(self, payment):
        payment_amount = payment['payment_amount']
        return str(payment_amount)

    def get_next_payment_date(self, subscription):
        # XXX Quitar cuando está desactivado
        _date = subscription['next_payment_date'] or ''
        return date_to_string(_date)

    def get_last_payment(self, subscription_id):
        query = """
            SELECT
                "pgp_pago"."fecha_pago" as payment_date,
                "pgp_pago"."pa_monto" as payment_amount
            FROM
                "pgp_pago"
            WHERE
                "pgp_pago"."psuscripcion_id" = %(subscription_id)s
            ORDER BY
                "pgp_pago"."id" DESC
            LIMIT 1;
        """
        with self.get_cursor() as cursor:
            cursor.execute(
                query,
                {'subscription_id': subscription_id}
            )
            last_payment = cursor.fetchone()

        return last_payment

    def get_next_payment_amount(self, subscription):
        query = """
            SELECT
                "payment_subscriptionpromotion"."amount",
                "payment_subscriptionpromotion"."state"
            FROM
                "payment_subscriptionpromotion"
            WHERE
                "payment_subscriptionpromotion"."psuscripcion_id" = %(subscription_id)s
            ORDER  BY
                "payment_subscriptionpromotion"."id" DESC
            LIMIT 1;
        """
        query_kwargs = {
            "subscription_id": subscription["subscription_id"]
        }

        with self.get_cursor() as cursor:
            cursor.execute(query, query_kwargs)
            promotion = cursor.fetchone()

        # Si la promoción está activa el precio es el de la promoción
        if promotion and promotion['state']:
            amount = str(promotion['amount'])

        # Caso contrario el precio es el de tips_precio
        else:
            amount = str(subscription['price'])

        return amount
