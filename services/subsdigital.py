from utils import date_to_string, timestamp_to_datetime

from .base import AutogestionDetailView, AutogestionListView


class SubscriptionDigitalMixin(object):

    def get_connection(self):
        """
            Retorna la conexión a la DB de paywall
        """
        return self.get_paywall_connection()

    def get_status(self, subscription):
        """
            Recibe los datos de la suscripción y convierte el estado de entero a string
        """

        code = subscription['status']

        status = ''
        if code == 1:
            status = 'Activo'

        elif code == 2:
            status = 'Terminado'

        elif code == 3:
            status = 'Cancelado'

        elif code == 4:
            status = 'Suspendido'

        return status

    def get_periodicity(self, subscription):
        """
            Recibe los datos de la suscripción y retorna la periodicidad
        """

        plan_data = subscription['plan_data']
        # XXX Manejar casos suscripciones sin plan_data
        frequency = plan_data['rates'][-1]['billingFrequency'] if plan_data else ''

        if frequency == 'Day':
            periodicity = 'Diario'

        elif frequency == 'Month':
            periodicity = 'Mensual'

        elif frequency == 'Year':
            periodicity = 'Anual'

        else:
            periodicity = ''

        return periodicity

    def get_delivery(self, subscription):
        """
            Recibe los datos de la suscripción y retorna su código delivery
        """
        query = '''
            SELECT
                "paywall_operation"."siebel_delivery"
            FROM   "paywall_operation"
                INNER JOIN "paywall_payment"
                        ON ( "paywall_operation"."payment_id" = "paywall_payment"."id" )
                INNER JOIN "paywall_subscription"
                        ON ( "paywall_payment"."subscription_id" =
                                "paywall_subscription"."id" )
            WHERE  ( "paywall_subscription"."arc_id" = %(subscription_id)s
                    AND "paywall_operation"."siebel_delivery" IS NOT NULL )
            LIMIT 1;
        '''
        query_kwargs = {'subscription_id': subscription['subscription_id']}

        with self.get_cursor() as cursor:
            cursor.execute(query, query_kwargs)
            operation = cursor.fetchone()

        delivery = operation['siebel_delivery'] if operation else ''

        return delivery


class SubscriptionDigitalListView(SubscriptionDigitalMixin, AutogestionListView):

    def get(self):
        """
        This function fetches content from MySQL RDS instance
        """
        subscriptions = self.get_objects()

        subscriptions_data = []
        for subscription in subscriptions:
            # https://stackoverflow.com/questions/6739355/
            subscriptions_data.append(
                {
                    'category': 'digital',
                    'delivery': self.get_delivery(subscription) or '',
                    'id': str(subscription['subscription_id']),
                    'periodicity': self.get_periodicity(subscription),
                    'portal': subscription['portal'],
                    'product': subscription['data']['productName'],
                    'status': self.get_status(subscription),
                }
            )

        return self.render(
            {'subscriptions': subscriptions_data}
        )

    def get_query(self):
        query = '''
            SELECT
                "paywall_subscription"."arc_id" as subscription_id,
                "paywall_subscription"."state" as status,
                "paywall_subscription"."data" as data,
                "paywall_plan"."data" as plan_data,
                "paywall_partner"."partner_code" as portal
            FROM   "paywall_subscription"
                INNER JOIN "paywall_partner"
                        ON ( "paywall_subscription"."partner_id" =
                            "paywall_partner"."id" )
                INNER JOIN "paywall_paymentprofile"
                        ON ( "paywall_subscription"."payment_profile_id" =
                                "paywall_paymentprofile"."id" )
                LEFT OUTER JOIN "paywall_plan"
                                ON ( "paywall_subscription"."plan_id" =
                                "paywall_plan"."id" )
            WHERE  (
                    LOWER("paywall_paymentprofile"."prof_doc_num") = LOWER(%(document_number)s)
        '''

        if self.clean_portal():
            query += ''' AND "paywall_partner"."partner_code" = %(portal)s '''

        query += ''' AND "paywall_paymentprofile"."prof_doc_type" = %(document_type)s ) ; '''

        return query


class SubscriptionDigitalDetailView(SubscriptionDigitalMixin, AutogestionDetailView):

    def get(self, subscription_id):
        """
        This function fetches content from MySQL RDS instance
        """
        subscription = self.get_object(
            subscription_id=subscription_id
        )

        data = {
            "category": "digital",
            "deliveryDays": "",
            "delivery": self.get_delivery(subscription),
            "email": self.get_email(subscription),
            "id": subscription_id,
            "lastPaymentAmount": self.get_last_payment_amount(subscription),
            "lastPaymentDate": self.get_last_payment_date(subscription),
            "name": self.get_product(subscription),
            "nextPaymentAmount": self.get_next_payment_amount(subscription),
            "nextPaymentDate": self.get_next_payment_date(subscription),
            "periodicity": self.get_periodicity(subscription),
            "price": self.get_price(subscription),
            "status": self.get_status(subscription),
        }
        return self.render(data)

    def get_query(self):
        query = '''
            SELECT
                "paywall_subscription"."arc_id" as subscription_id,
                "paywall_subscription"."data" as data,
                "paywall_subscription"."state" as status,
                "paywall_plan"."data" as plan_data,
                "paywall_partner"."partner_code" as portal,
                "arcsubs_arcuser"."email" as email
            FROM   "paywall_subscription"
                LEFT OUTER JOIN "paywall_plan"
                                ON ( "paywall_subscription"."plan_id" =
                                "paywall_plan"."id" )
                LEFT OUTER JOIN "paywall_partner"
                                ON ( "paywall_subscription"."partner_id" =
                                    "paywall_partner"."id" )
                LEFT OUTER JOIN "arcsubs_arcuser"
                                ON ( "paywall_subscription"."arc_user_id" =
                                    "arcsubs_arcuser"."id" )
            WHERE  "paywall_subscription"."arc_id" = %(subscription_id)s
            LIMIT 1;
        '''
        return query

    def get_last_payment_amount(self, subscription):
        subscription_data = subscription['data']
        last_order = subscription_data['salesOrders'][-1] if subscription_data else {}
        amount = last_order.get('total', '')
        if isinstance(amount, (float, int)):
            amount = "{:.2f}".format(amount)
        return amount

    def get_last_payment_date(self, subscription):
        subscription_data = subscription['data']
        last_order = subscription_data['salesOrders'][-1] if subscription_data else {}
        timestamp = last_order.get('orderDateUTC', '')
        return date_to_string(timestamp_to_datetime(timestamp))

    def get_next_payment_amount(self, subscription):
        plan_data = subscription['plan_data']
        subscription_data = subscription['data']
        cycle = subscription_data.get('currentRetailCycleIDX')
        # XXX rates cuando no tiene plan_data
        rates = plan_data.get('rates')  # Según priceCode
        return self.calculate_next_payment(cycle, rates) or ''

    def calculate_next_payment(self, cycle, rates):
        """
            Calcula el monto a pagar del mes.
            https://arcpublishing.atlassian.net/servicedesk/customer/portal/2/ACS-16111
        """

        if not cycle or not rates:
            return None

        last_rate = None
        retail_cycle = 0
        for rate in rates:
            retail_cycle += rate['durationCount']
            if retail_cycle > cycle:
                if not last_rate:
                    last_rate = rate
                break
            last_rate = rate  # Debe asignarse al final

        return last_rate['amount']

    def get_next_payment_date(self, subscription):
        subscription_data = subscription['data']
        timestamp = subscription_data.get('nextEventDateUTC', '')
        return date_to_string(timestamp_to_datetime(timestamp))

    def get_price(self, subscription):
        return self.get_next_payment_amount(subscription)

    def get_email(self, subscription):
        """
            Retorna el email de login del usuario
        """
        return subscription['email']

    def get_product(self, subscription):
        return subscription['data']['productName']
