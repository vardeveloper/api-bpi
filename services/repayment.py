from .base import SiebelBaseView


class RepaymentListBaseView(SiebelBaseView):

    def get(self, subscription_id):
        data = {
            'repayments':  [
                {
                    'amount': '0,00',
                    'createDate': 'dd/mm/aa',
                    'depositDate': 'dd/mm/aa',
                    'bank': 'Banco X',
                    'bankAccount': 'xxxxxxxxxx1234',
                    'CCI': 'xxxxxxxxxxxx',
                    'deposited': False,
                    'rejected': False,
                }
            ]
        }
        return self.render(data)


class SubscriptionPrintRepaymentListView(RepaymentListBaseView):
    pass

class SubscriptionDigitalRepaymentListView(RepaymentListBaseView):
    pass

class SubscriptionBundleRepaymentListView(RepaymentListBaseView):
    pass
