from .base import AutogestionDetailView


class SubscriptionClubDetailView(AutogestionDetailView):

    def get_connection(self):
        return self.get_club_connection()

    def get_query(self):
        query = '''
            SELECT
                epa_producto.id,
                epa_producto.codigo_suscriptor,
                epa_producto.nombres,
                epa_producto.apellido_paterno,
                epa_producto.apellido_materno,
                epa_producto.tipo_documento,
                epa_producto.numero_documento,
                epa_producto.email_contacto,
                Count(*) AS cantidad_de_suscripciones,
                epa_suscripcion.codigo_producto AS codigo_productos_suscriptor
            FROM   suscriptor_producto epa_suscripcion
                JOIN suscriptor epa_producto
                    ON epa_suscripcion.codigo_suscriptor = epa_producto.codigo_suscriptor
            WHERE
                (
                epa_suscripcion.estado = 1
                AND epa_producto.tipo_documento=%(document_type)s
                AND epa_producto.numero_documento=%(document_number)s
                AND epa_suscripcion.codigo_producto IN ( 20, 19, 'E', 'T', 'B', 1 )
                )
            GROUP BY epa_producto.id;
            -- 19, 20 Digitales
            -- E, T, 1, B Impresos
        '''
        return query

    def get(self):
        """
        This function fetches content from MySQL RDS instance
        """
        subscription = self.get_object()

        return self.render(
            {'subscription': subscription}
        )

    def get_status(self, subsciber_id):
        pass

    def get_benefits(self, subsciber_id):
        pass

    def get_query_kwargs(self):
        return {
            'document_type': self.clean_document_type(),
            'document_number': self.clean_document_number(),
        }
