from utils import date_to_string, timestamp_to_datetime

from .base import AutogestionDetailView, SiebelBaseView


class SubscriptionPrintAnnulationView(SiebelBaseView):

    def get(self, subscription_id):

        subscription = self.get_subscription_data(subscription_id)

        if subscription['Estado'] in ('Baja Potencial', 'Suspensión Temporal'):
            data = {
                'status': 'Pendiente',
                'date': subscription['FchProxPago'],
                'subscriptionStatus': subscription['Estado'],
            }

        elif subscription['Estado'] in ('Inactivo', 'No Efectiva'):
            data = {
                'status': 'Terminado',
                'date': subscription['FchUltPago'],
                'subscriptionStatus': subscription['Estado'],
            }

        else:
            # XXX Pendiente API
            data = {
                'status': '',
                'date': '',
                'subscriptionStatus': subscription['Estado'],
            }

        return self.render(data)


class SubscriptionBundleAnnulationView(AutogestionDetailView):

    def get_connection(self):
        return self.get_subsonline_connection()

    def get_query(self):
        query = '''
            SELECT
                "payment_psuscripcion"."id" as subscription_id,
                "payment_psuscripcion"."estado" as status,
                "payment_psuscripcion"."siebel_delivery" as delivery,
                "payment_psuscripcion"."fecha_cargo" as next_payment_date,
                "pgp_tipo_suscripcion"."tips_nombre" as periodicity,
                "pgp_tipo_suscripcion"."tips_precio" as price,
                "epa_producto"."prod_nombre" as product,
                "epa_producto_detalle"."pdet_dias_semana" as days
            FROM   "payment_psuscripcion"
                INNER JOIN "pgp_perfil_pago"
                        ON ( "payment_psuscripcion"."perfil_pago_id" =
                                "pgp_perfil_pago"."id" )
                INNER JOIN "sis_partners"
                        ON ( "payment_psuscripcion"."partner_id" = "sis_partners"."id" )
                LEFT OUTER JOIN "payment_plan"
                                ON ( "payment_psuscripcion"."plan_id" =
                                "payment_plan"."id" )
                LEFT OUTER JOIN "pgp_tipo_suscripcion"
                                ON ( "payment_plan"."tips_id" =
                                "pgp_tipo_suscripcion"."id" )
                LEFT OUTER JOIN "epa_producto"
                                ON ( "pgp_tipo_suscripcion"."prod_id" =
                                "epa_producto"."id" )
                LEFT OUTER JOIN "epa_producto_detalle"
                                ON ( "epa_producto_detalle"."prod_id" =
                                "epa_producto"."id" )
            WHERE  ( "payment_psuscripcion"."id" = %(subscription_id)s ) LIMIT 1;
        '''
        return query

    def get(self, subscription_id):
        # XXX Implementar lógica
        data = {
            'status': '',
            'date': ''
        }
        return self.render(data)


class SubscriptionDigitalAnnulationView(AutogestionDetailView):

    def get_connection(self):
        return self.get_paywall_connection()

    def get_query(self):
        query = '''
            SELECT "paywall_subscription"."arc_id" as subscription_id,
                "paywall_subscription"."data" as data
            FROM   "paywall_subscription"
            WHERE  "paywall_subscription"."arc_id" = %(subscription_id)s
            LIMIT 1;
        '''
        return query

    def get(self, subscription_id):
        subscription = self.get_object(
            subscription_id=subscription_id
        )

        # XXX Implementar lógica
        data = {
            'status': self.get_status(subscription),
            'date': self.get_date(subscription),
        }
        return self.render(data)

    def get_status(self, subscription):
        status = ''
        data = subscription['data']
        if data['status'] == 3:  # Cancelado
            status = 'Pendiente'
        elif data['status'] == 2:  # Terminado
            status = 'Terminado'
        return status

    def get_date(self, subscription):
        _date = ''
        data = subscription['data']
        if data['status'] == 3:  # Cancelado
            # XXX Validar fecha zona horaria
            _date = date_to_string(
                timestamp_to_datetime(data['nextEventDateUTC'])
            )
        elif data['status'] == 2:  # Terminado
            for event in data['events']:
                if event['eventType'] == 'TERMINATE_SUBSCRIPTION':
                    _date = date_to_string(
                        timestamp_to_datetime(event['eventDateUTC'])
                    )
                    break
        return _date
