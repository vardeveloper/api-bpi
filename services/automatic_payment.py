from .base import AutogestionDetailView


class SubscriptionPrintAutomaticPaymentView(AutogestionDetailView):

    def get(self, subscription_id):
        # XXX Pendiente API
        data = {
            'cardNumber': '**** **** **** 1234',
            'date': '',
            'failRenew': False,
        }
        return self.render(data)


class SubscriptionBundleAutomaticPaymentView(AutogestionDetailView):

    def get(self, subscription_id):
        # XXX Implementar lógica
        data = {
            'cardNumber': '',
            'date': '',
            'failRenew': False,
        }
        return self.render(data)


class SubscriptionDigitalAutomaticPaymentView(AutogestionDetailView):

    def get_connection(self):
        return self.get_paywall_connection()

    def get_query(self):
        query = '''
            SELECT "paywall_subscription"."arc_id" as subscription_id,
                "paywall_subscription"."data" as data
            FROM   "paywall_subscription"
            WHERE  "paywall_subscription"."arc_id" = %(subscription_id)s
            LIMIT 1;
        '''
        return query

    def get(self, subscription_id):
        """
        This function fetches content from MySQL RDS instance
        """
        subscription = self.get_object(
            subscription_id=subscription_id
        )

        data = {
            'cardNumber': self.get_carnumber(subscription),
            'date': '',
            'failRenew': self.get_failrenew(subscription),
        }
        return self.render(data)

    def get_carnumber(self, subscription):
        data = subscription['data']
        payment_method = data.get('currentPaymentMethod', '')
        if 'lastFour' in payment_method:
            return "**** **** **** %s" % payment_method['lastFour']

    def get_failrenew(self, subscription):
        data = subscription['data']
        return data['status'] == 4  # Suspendido
