"""
    Handlers de APIs suscripciones print
"""

from flask import abort

from .base import SiebelBaseView


class SiebelNumberView(SiebelBaseView):
    """
        Controlador para convertir número delivery en número siebel
    """

    def get(self, subscription_id):
        """ https://stackoverflow.com/questions/10434599/ """

        subscription = self.get_subscription_data(subscription_id)

        if not subscription:
            abort(404, description="Suscripción no encontrada")

        data = {
            "category": subscription['Tipo'].lower(),
            "delivery": subscription_id,
            "id": subscription_id,
            "name": subscription['DescPaquete'],
            "periodicity": subscription['Periodo'].title(),
            "price": str(subscription['PrecioActual']),
            "siebelNumber": subscription['NroSiebel'] or '',
            "status": subscription['Estado'].title(),
        }
        return self.render(data)


class SubscriptionPrintListView(SiebelBaseView):
    """
        Controlador para listar las suscripciones print por tipo y número de documento
    """

    def get(self):
        """ https://stackoverflow.com/questions/10434599/ """

        portal=self.clean_portal()
        _, subscriptions = self.get_subscriber_data()

        subscriptions_data = self.get_subscriptions_data(
            portal=portal,
            subscriptions=subscriptions
        )

        return self.render(
            {'subscriptions': subscriptions_data}
        )

    def get_subscriptions_data(self, portal, subscriptions):

        subscriptions_data = []
        for subscription in subscriptions:

            subscription_portal = self.get_subscription_portal(subscription)

            # Sólo se agregan paquetes de EL COMERCIO y GESTION
            if not subscription_portal:
                continue

            # Filtro por portal
            if portal and subscription_portal != portal:
                continue

            # No se agregan paquetes bundle
            if 'DIGITAL' in subscription['DescPaquete'].upper():
                continue

            # Sólo se listan suscripciones print
            if 'PRINT' not in subscription['Tipo'].upper():
                continue

            subscription_data = self.get_subscription_data(subscription['CodDelivery'])

            status = subscription_data.get('Estado') or ''
            siebel_number = subscription_data.get('NroSiebel') or ''
            siebel = str(subscription['CodDelivery'])

            subscriptions_data.append(
                {
                    'category': 'print',
                    'delivery': siebel,
                    'id': siebel,
                    'periodicity': subscription['Periodo'].title(),
                    'portal': subscription_portal,
                    'product': subscription['DescPaquete'],
                    'siebelNumber': siebel_number,
                    'status': status.title(),
                }
            )

        return subscriptions_data

    def get_subscription_portal(self, subscription):

        if subscription['Portal'] == 'GESTION':
            subscription_portal = 'gestion'
        elif subscription['Portal'] == 'EL COMERCIO':
            subscription_portal = 'elcomercio'
        else:
            subscription_portal = ''

        return subscription_portal


class SubscriptionPrintDetailView(SiebelBaseView):
    """
        Controlador de detalle de suscripción print
    """

    def get(self, subscription_id):

        subscription = self.get_subscription_data(subscription_id)

        if not subscription:
            abort(404, description="Suscripción no encontrada")

        # XXX "Portal": "EL COMERCIO"
        data = {
            "address": subscription.get('DirecReparto') or '',
            "category": subscription['Tipo'].lower(),
            "delivery": subscription_id,
            "deliveryDays": subscription.get('DiasReparto') or '',
            "district": subscription.get('DistReparto') or '',
            "email": "",
            "id": subscription_id,
            "lastPaymentAmount": str(subscription['MntUltPago']),
            "lastPaymentDate": subscription['FchUltPago'],
            "name": subscription['DescPaquete'],
            "nextPaymentAmount": "",  # XXX Mock
            "nextPaymentDate": subscription['FchProxPago'],
            "periodicity": subscription['Periodo'].title(),
            "price": str(subscription['PrecioActual']),
            "siebelNumber": subscription['NroSiebel'] or '',
            "status": subscription['Estado'].title(),
        }
        return self.render(data)
