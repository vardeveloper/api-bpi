from .subsdigital import SubscriptionDigitalDetailView


def test_calculate_next_payment():
    handler = SubscriptionDigitalDetailView()

    rates = [
        {
        "amount": "5.00",
        "duration": "Month",
        "billingCount": 1,
        "durationCount": 3,
        "billingFrequency": "Month"
        },
        {
        "amount": "20.00",
        "duration": "UntilCancelled",
        "billingCount": 1,
        "durationCount": 1,
        "billingFrequency": "Month"
        }
    ]
    assert handler.calculate_next_payment(1, rates) == "5.00"
    assert handler.calculate_next_payment(2, rates) == "5.00"
    assert handler.calculate_next_payment(3, rates) == "5.00"
    assert handler.calculate_next_payment(4, rates) == "20.00"
    assert handler.calculate_next_payment(5, rates) == "20.00"
    assert handler.calculate_next_payment(6, rates) == "20.00"

    rates = [
        {
            "amount": "19.00",
            "duration": "Month",
            "billingCount": 1,
            "durationCount": 1,
            "billingFrequency": "Month"
        },
        {
            "amount": "39.00",
            "duration": "UntilCancelled",
            "billingCount": 1,
            "durationCount": 1,
            "billingFrequency": "Month"
        }
    ]
    assert handler.calculate_next_payment(1, rates) == "19.00"
    assert handler.calculate_next_payment(2, rates) == "39.00"
    assert handler.calculate_next_payment(3, rates) == "39.00"
    assert handler.calculate_next_payment(4, rates) == "39.00"
