"""
    Controladores para el API de peruquiosco
"""

import logging
import os
import random
import string

import requests
from flask import request
from sentry_sdk import capture_event

from .base import AutogestionDetailView, AutogestionListView

logger = logging.getLogger()
logger.setLevel(os.getenv('LOG_LEVEL', 'WARNING'))


class PeruQuioscoChangePasswordView(AutogestionDetailView):
    """
        Controlador del API peruquiosco para cambiar contraseña
    """
    def get_connection(self):
        return self.get_peruquiosco_connection()

    def post(self):
        email = self.clean_email()
        password = self.get_password()

        result = self.set_password(
            email=email,
            password=password
        )

        if result.get('success'):
            data = {
                'success': True,
                'newPassword': password,
                'email': email,
                'message': result.get('message'),
            }

        else:
            data = {
                'success': False,
                'message': result.get('message'),
            }

        logger.info('%s %s', email, result.get('success'))
        return self.render(data)

    def get_password(self):
        return request.values.get('password') or self.generate_password(length=8)

    def generate_password(self, length):
        letters = string.ascii_lowercase
        result_str = ''.join(random.choice(letters) for i in range(length))
        # result_str = 'pq.2021.' + random.choice(letters)
        return result_str

    def get_peruid_keys(self):
        query = """ SELECT * FROM sys_peruid WHERE pid_host = 'peruquiosco.pe'; """

        with self.get_cursor() as cursor:
            cursor.execute(query)
            obj = cursor.fetchone() or {}

        return obj.get('secret_key'), obj.get('pid_key')

    def get_peruid_url(self, endpoint):
        url = os.getenv('PERUID_URL', '') + endpoint
        return url

    def get_hash(self, email):
        email = email.replace('@', 'arroba')
        endpoint = '/index.php/service/usuarios/rest_solicita_rename_password' \
            '/secretkey/{secret_key}/apikey/{api_key}/correo/{email}/'

        secret_key, api_key = self.get_peruid_keys()

        endpoint = endpoint.format(
            secret_key=secret_key,
            api_key=api_key,
            email=email,
        )

        url = self.get_peruid_url(endpoint=endpoint)

        response = requests.get(url)

        if response.status_code == 200:
            result = response.json()
            return result.get('hash')

    def set_password(self, email, password):
        endpoint = '/index.php/service/usuarios/rest_new_password' \
            '/secretkey/{secret_key}/apikey/{api_key}'\
            '/correo/{email}/hash/{hash}/password/{password}'

        secret_key, api_key = self.get_peruid_keys()

        endpoint = endpoint.format(
            secret_key=secret_key,
            api_key=api_key,
            email=email,
            hash=self.get_hash(email),
            password=password,
        )

        url = self.get_peruid_url(endpoint=endpoint)

        response = requests.get(url)
        if response.status_code == 200:
            result = response.json()
        else:
            result = {}
            capture_event(
                {
                    'message': 'PQ.set_password %s %s' % (
                        endpoint, response.status_code),
                    'extra': {
                        'endpoint': endpoint,
                        'response': response.text,
                        'url': url,
                    }
                }
            )

        return result


class PeruQuioscoListView(AutogestionListView):

    def get_connection(self):
        return self.get_peruquiosco_connection()

    def get_query(self):
        query = '''
            SELECT
                `epa_suscripcion`.`sus_id`,
                `epa_suscripcion`.`is_recurrence`,
                `epa_suscripcion`.`sus_impreso_paq_actual`,
                `epa_suscripcion`.`sus_modalidad_actual`,
                `epa_suscripcion`.`sus_tipo_paquete`,
                IF(
                    epa_suscripcion.sus_fec_caducidad > NOW(),
                    'Activo',
                    'Terminado'
                    ) AS `sus_activo`,
                IF(
                    sus_fh_edicion IS NULL,
                    DATE_FORMAT(
                        sus_fec_inicio,
                        '%%d/%%m/%%Y %%h:%%i %%p'),
                    DATE_FORMAT(
                        sus_fh_edicion,
                        '%%d/%%m/%%Y %%h:%%i %%p')
                    ) AS `fecha_inicio`,
                IF(
                    YEAR(epa_suscripcion.sus_fec_caducidad) = '9999',
                    'Automático',
                    DATE_FORMAT(
                        epa_suscripcion.sus_fec_caducidad,
                        '%%d/%%m/%%Y')
                    ) AS `fecha_caducidad`,
                `epa_modalidad_suscripcion`.`mod_nombre`,
                `epa_producto`.`prod_id`,
                `epa_producto`.`prod_nombre`,
                `epa_producto`.`prod_codigo`,
                `auth_usuario`.`us_id`,
                `auth_usuario`.`us_email`,
                `auth_usuario`.`us_confirmado`,
                `auth_usuario`.`us_estado`,
                `auth_usuario`.`us_nombres`,
                `auth_usuario`.`us_peruid_ecoid`,
                `auth_usuario`.`us_cod_sus_impresa`,
                `auth_usuario`.`documento_tipo_codigo`,
                `auth_usuario`.`us_docnumero_sus_impresa`
            FROM
                `epa_suscripcion`
                INNER JOIN
                    `epa_producto` ON epa_suscripcion.producto_id = epa_producto.prod_id
                INNER JOIN
                    `epa_modalidad_suscripcion` ON
                        epa_suscripcion.sus_modalidad_actual = epa_modalidad_suscripcion.mod_id
                INNER JOIN
                    `auth_usuario` ON epa_suscripcion.usuario_id = auth_usuario.us_id
                LEFT JOIN
                    `pgp_perfil_pago` ON pgp_perfil_pago.tb_users_id = auth_usuario.us_id
            WHERE
                epa_suscripcion.sus_tipo_paquete = 1 AND
                (epa_producto.prod_suscripcion = 1 OR epa_producto.prod_codigo = 'revistaggratis')
        '''

        user_ids = self.get_user_ids()
        if not user_ids:
            query += '''
                AND {}
                LIMIT 100;
            '''

        elif len(user_ids) == 1:
            query += '''
                AND (`auth_usuario`.us_id = %s OR ({}))
                LIMIT 100;
            ''' % user_ids[0]

        elif len(user_ids) > 1:
            ids = ','.join(user_ids)
            query += '''
                AND (`auth_usuario`.us_id IN (%s) OR ({}))
                LIMIT 100;
            ''' % ids

        query = query.format('''
            `auth_usuario`.`documento_tipo_codigo` = %(document_type)s AND
            `auth_usuario`.`us_docnumero_sus_impresa` = %(document_number)s
        ''')

        return query

    def get_user_ids(self):
        query = '''
            SELECT
                `pgp_perfil_pago`.`tb_users_id` as user_id
            FROM
                `pgp_perfil_pago`
            WHERE
                `pgp_perfil_pago`.`tipo_doc` = %(document_type)s AND
                `pgp_perfil_pago`.`nro_doc` = %(document_number)s
            LIMIT 100;
        '''
        query_kwargs = self.get_query_kwargs()

        with self.get_cursor() as cursor:
            cursor.execute(query, query_kwargs)
            users = cursor.fetchall()

        ids = []
        if users:
            ids = [str(user['user_id']) for user in users]

        return ids

    def get(self):
        """
        This function fetches content from MySQL RDS instance
        """
        logger.info('START get_objects')
        subscriptions = self.get_objects()
        logger.info('END get_objects')

        subscriptions_ids = []
        subscriptions_data = []
        for subscription in subscriptions:
            logger.info('START get_last_payment')
            last_payment = self.get_last_payment(subscription)
            logger.info('END get_last_payment')

            payment_amount = str(
                last_payment.get('pa_monto') or
                last_payment.get('ope_precio')
            )

            origin = {}
            delivery = ''
            controller = ''
            if last_payment.get('pa_id'):
                if last_payment.get('subs_id'):
                    controller = 'peruquiosco'
                    delivery = last_payment.get('siebel_delivery', '')
                else:
                    delivery = last_payment.get('ope_delivery', '')

                if last_payment.get('pa_medio_pago') == 'SISAC':
                    controller = 'siebel'

                elif last_payment.get('pa_medio_pago') in ('VISA', 'PE'):
                    controller = 'peruquiosco'

                if not delivery:
                    logger.info('START get_delivery_by_pago_id')
                    delivery = self.get_delivery_by_pago_id(last_payment.get('pa_id'))
                    logger.info('END get_delivery_by_pago_id')

                if not controller:
                    capture_event(
                        {
                            'message': 'PQ.ConPagoSinController WARNING',
                            'extra': {
                                'query_kawargs': self.get_query_kwargs(),
                                'subscription': subscription,
                                'last_payment': last_payment,
                            }
                        }
                    )

            else:
                if last_payment.get('ope_origen') == 'SONLINE':
                    controller = 'subsonline'
                    capture_event(
                        {
                            'message': 'PQ.SONLINE DEBUG',
                            'extra': {
                                'query_kawargs': self.get_query_kwargs(),
                                'subscription': subscription,
                                'last_payment': last_payment,
                            }
                        }
                    )

                elif last_payment.get('impreso_delivery'):
                    logger.info('START get_origin')
                    controller = 'print'
                    origin = self.get_origin(last_payment.get('impreso_delivery'))
                    logger.info('END get_origin')

                else:
                    controller = ''
                    capture_event(
                        {
                            'message': 'PQ.SinPagoSinController WARNING',
                            'extra': {
                                'query_kawargs': self.get_query_kwargs(),
                                'subscription': subscription,
                                'last_payment': last_payment,
                            }
                        }
                    )

            subscription_id = str(subscription['sus_id'])
            if subscription_id not in subscriptions_ids:
                subscriptions_ids.append(subscription_id)
                subscriptions_data.append(
                    {
                        "id": str(subscription_id),
                        "delivery": delivery,
                        "controller": controller,
                        'category': 'peruquiosco',
                        "email": subscription['us_email'],
                        "emailVerified": bool(subscription['us_confirmado']),
                        "lastPaymentAmount": payment_amount,
                        "lastPaymentDate": last_payment.get('fecha_inicio', ''),
                        "nextPaymentAmount": payment_amount,
                        "nextPaymentDate": self.get_next_payment_date(subscription),
                        "productName": subscription['prod_nombre'],
                        "status": subscription['sus_activo'],
                        'periodicity': str(last_payment.get('ope_tis_desc', '')),
                        "origin": origin,
                        'info1': str(subscription),
                        'info2': str(last_payment),
                    }
                )

        return self.render(
            {
                'subscriptions': subscriptions_data,
            }
        )

    def get_delivery_by_pago_id(self, pago_id):
        query_kwargs = {
            'pago_id': pago_id,
        }
        query = '''
            SELECT
                `epa_operacion`.`ope_id`,
                `epa_operacion`.`ope_precio`,
                `epa_operacion`.`ope_origen`,
                `epa_operacion`.`ope_tis_desc`,
                `pgp_pago_operacion`.`pgo_id`,
                `pgp_pago_operacion`.`pago_id`,
                `pgp_pago_operacion`.`operacion_id`,
                `pgp_pago`.`pa_id`,
                `pgp_pago`.`pa_monto`,
                `pgp_pago`.`pa_medio_pago`,
                `epa_operacion`.`cod_sus_impreso` as impreso_delivery,
                `epa_operacion_ov`.`opeo_codigo_suscripcion` as `ope_delivery`,
                `payment_suscripcion`.`subs_id`,
                `payment_suscripcion`.`siebel_delivery` as `siebel_delivery`
            FROM
                `epa_operacion`
            LEFT JOIN `epa_operacion_ov`
                ON ( `epa_operacion_ov`.`operacion_id` = `epa_operacion`.`ope_id` )
            LEFT JOIN `pgp_pago_operacion`
                ON ( `pgp_pago_operacion`.`operacion_id` = `epa_operacion`.`ope_id` )
            LEFT JOIN `pgp_pago`
                ON ( `pgp_pago`.`pa_id` = `pgp_pago_operacion`.`pago_id` )
            LEFT JOIN `payment_suscripcion`
                ON ( `payment_suscripcion`.`subs_id` = `pgp_pago`.`subs_id` )
            WHERE
                `epa_operacion`.`ope_estado` = 1 AND  -- ESTADO_ACEPTADO o PAGADO = 1
                `pgp_pago`.`pa_id` = %(pago_id)s
        '''

        with self.get_cursor() as cursor:
            cursor.execute(query, query_kwargs)
            payments = cursor.fetchall()

        delivery = ''
        for payment in payments:
            delivery = (
                payment.get('siebel_delivery') or
                payment.get('ope_delivery') or ''
            )
            if delivery:
                break

        return delivery or ''

    def get_last_payment(self, subscription):
        query_kwargs = {
            'usuario_id':subscription['us_id'],
            'producto_id':subscription['prod_id'],
        }
        query = '''
            SELECT
                `epa_operacion`.`ope_id`,
                `epa_operacion`.`cod_sus_impreso` as impreso_delivery,
                `epa_operacion`.`ope_estado`,
                `epa_operacion`.`ope_origen`,
                `epa_operacion`.`ope_precio`,
                `epa_operacion`.`ope_tis_desc`,
                `pgp_pago_operacion`.`pgo_id`,
                `pgp_pago_operacion`.`pago_id`,
                `pgp_pago_operacion`.`operacion_id`,
                `pgp_pago`.`pa_id`,
                `pgp_pago`.`pa_monto`,
                `pgp_pago`.`pa_medio_pago`,
                `epa_operacion_ov`.`opeo_codigo_suscripcion` as `ope_delivery`,
                `payment_suscripcion`.`subs_id`,
                `payment_suscripcion`.`siebel_delivery` as `siebel_delivery`,
                DATE_FORMAT(
                    `ope_fec_inicio`,
                    '%%d/%%m/%%Y %%h:%%i %%p') AS `fecha_inicio`
            FROM
                `epa_operacion`
            LEFT JOIN `epa_operacion_ov`
                ON ( `epa_operacion_ov`.`operacion_id` = `epa_operacion`.`ope_id` )
            LEFT JOIN `pgp_pago_operacion`
                ON ( `pgp_pago_operacion`.`operacion_id` = `epa_operacion`.`ope_id` )
            LEFT JOIN `pgp_pago`
                ON ( `pgp_pago`.`pa_id` = `pgp_pago_operacion`.`pago_id` )
            LEFT JOIN `payment_suscripcion`
                ON ( `payment_suscripcion`.`subs_id` = `pgp_pago`.`subs_id` )
            WHERE
                `epa_operacion`.`ope_estado` = 1 AND  -- ESTADO_ACEPTADO: 1
                `epa_operacion`.`producto_id` = %(producto_id)s AND
                `epa_operacion`.`usuario_id` = %(usuario_id)s
            ORDER  BY
                `epa_operacion`.`ope_fec_creacion` DESC
            LIMIT 1;
        '''

        with self.get_cursor() as cursor:
            cursor.execute(query, query_kwargs)
            payment = cursor.fetchone()

        return payment or {}

    def get_next_payment_date(self, subscription):
        next_payment_date = subscription['fecha_caducidad']
        return next_payment_date

    def get_origin(self, delivery):
        if not delivery:
            return {}

        autogestion_url = os.getenv('AUTOGESTION_API_URL', '')
        autogestion_token = os.getenv('AUTOGESTION_TOKEN', '')
        url = autogestion_url + '/api/v1/subscription-print/%s/details' % delivery
        headers = {
            'Authorization': 'Bearer %s' % autogestion_token
        }

        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            result = response.json()
        else:
            capture_event(
                {
                    'message': 'PeruQuioscoListView.get_origin',
                    'extra': {
                        'AUTOGESTION_API_URL': autogestion_url,
                        'response': response.text,
                        'url': url,
                    }
                }
            )
            result = {'delivery': delivery}

        return result
