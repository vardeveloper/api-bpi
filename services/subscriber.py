import os

import requests
from flask import abort
from sentry_sdk import capture_event
from utils import is_production

from .base import SiebelBaseView


class SubscriberDetailView(SiebelBaseView):

    def get(self):
        """
        This function fetches content from MySQL RDS instance
        https://stackoverflow.com/questions/4940670/
        """
        subscriber, subscriptions = self.get_subscriber_data()

        if not subscriber:
            self.get_peruquiosco_subscriber()
            abort(404, description="Suscriptor no encontrado")

        second_phone = self.get_phone(subscriber, 'Telefono2')
        if not second_phone:
            second_phone = self.get_paywall_phone()

        data = {
            'address': subscriber['Direccion'] or '',
            'dateOfBirth': subscriber['FechaNacimiento'] or '',
            'district': subscriber['Distrito'] or '',
            'document_number': subscriber['NroDocumento'],
            'document_type': subscriber['TipoDocumento'],
            'email': self.get_email(subscriber),
            'first_name': subscriber['Nombres'],
            'gender': subscriber['Sexo'] or '',
            'id': subscriber['CodigoSuscriptor'],
            'last_name': subscriber['ApellidoPaterno'],
            'phone': self.get_phone(subscriber, 'Telefono1'),
            'profession': subscriber['Profesion'] or '',
            'secondPhone': second_phone,
            'second_last_name': subscriber['ApellidoMaterno'],
            'subscriptions': subscriptions,
        }
        return self.render(data)

    def get_peruquiosco_subscriber(self):
        query = """
            SELECT *
            FROM pgp_perfil_pago
            WHERE
                tipo_doc = %(document_type)s AND
                nro_doc = %(document_number)s AND
                ente_code IS NOT NULL
            LIMIT 1;
        """
        query_kwargs = {
            'document_type': self.clean_document_type(),
            'document_number': self.clean_document_number(),
        }

        subscriber = None
        connection = self.get_peruquiosco_connection()
        with self.get_cursor(connection=connection) as cursor:
            cursor.execute(
                query,
                query_kwargs
            )
            subscriber = cursor.fetchone()
        connection.close()

        if subscriber:
            capture_event(
                {
                    'message': 'SubscriberDetailView.peruquioscoSubscriberExists ERROR',
                    'extra': {
                        'documentType': self.clean_document_type(),
                        'documentNumber': self.clean_document_number(),
                    }
                }
            )

        return subscriber

    def get_paywall_phone(self):
        query = """
            SELECT *
            FROM paywall_paymentprofile
            WHERE
                prof_doc_type = %(document_type)s AND
                prof_doc_num = %(document_number)s AND
                prof_phone IS NOT NULL AND
                siebel_entecode IS NOT NULL
            LIMIT 1;
        """
        query_kwargs = {
            'document_type': self.clean_document_type(),
            'document_number': self.clean_document_number(),
        }

        connection = self.get_paywall_connection()
        with self.get_cursor(connection=connection) as cursor:
            cursor.execute(
                query,
                query_kwargs
            )
            profile = cursor.fetchone()
        connection.close()

        phone = profile['prof_phone'] if profile else ''
        return phone or ''

    def get_email(self, subscriber):
        email = subscriber.get('Email')

        if email and not is_production():
            email = '%s@mailinator.com' % self.clean_document_number()
            email = email.lower()

        return email or ''

    def get_phone(self, subscriber, phone_name):
        phone = subscriber[phone_name] or ''
        phone = phone.replace('.0000000', '')
        return phone


class SubscriberFullDetailView(SiebelBaseView):

    AUTOGESTION_API_URL = os.getenv('AUTOGESTION_API_URL', '')
    AUTOGESTION_TOKEN = os.getenv('AUTOGESTION_TOKEN', '')

    def get(self):
        subscriptions = []

        subscriber = self.send_request('/api/v1/subscriber')

        if not subscriber:
            abort(404, description="Subscriber not found")

        delivery_list = []

        if subscriber.get('subscriptions'):
            for subscription in subscriber.get('subscriptions'):
                if subscription.get('CodDelivery'):
                    delivery_list.append(
                        subscription.get('CodDelivery')
                    )

        else:
            result = self.send_request('/api/v1/subscriptions/print')
            if result:
                subscriptions += result['subscriptions']

            result = self.send_request('/api/v1/subscriptions/bundle')
            if result:
                subscriptions += result['subscriptions']

            result = self.send_request('/api/v1/subscriptions/digital')
            if result:
                subscriptions += result['subscriptions']

            result = self.send_request('/api/v1/subscriptions/peruquiosco')
            if result:
                subscriptions += result['subscriptions']

            subscriber['subscriptions'] = subscriptions

            for subscription in subscriptions:
                if subscription.get('delivery'):
                    delivery_list.append(
                        subscription['delivery']
                    )

        subscriber['delivery'] = subscriber['siebelNumber'] = ''
        for delivery in delivery_list:
            subscription = self.get_subscription_data(delivery)
            if subscription and subscription['NroSiebel']:
                subscriber['delivery'] = delivery
                subscriber['siebelNumber'] = subscription['NroSiebel']
                break

        return self.render(subscriber)

    def send_request(self, endpoint):
        url = self.AUTOGESTION_API_URL + endpoint
        payload = {
            'document_number': self.clean_document_number(),
            'document_type': self.clean_document_type(),
        }
        headers = {
            'Authorization': 'Bearer %s' % self.AUTOGESTION_TOKEN
        }

        response = requests.get(
            url, params=payload, headers=headers
        )
        if response.status_code == 200:
            result = response.json()
        else:
            result = {}
            capture_event(
                {
                    'message': 'SubscriberFullDetailView.send_request %s %s' % (
                        endpoint, response.status_code),
                    'extra': {
                        'endpoint': endpoint,
                        'payload': payload,
                        'response': response.text,
                        'url': url,
                    }
                }
            )

        return result
