import json
import os

import psycopg2  # https://github.com/jkehler/awslambda-psycopg2
import pymysql
import requests
from flask import Response, abort, request
from flask.views import MethodView
from psycopg2.extensions import connection as PostgresConnection
from psycopg2.extras import RealDictCursor
from pymysql.connections import Connection as MysqlConnection
from pymysql.cursors import DictCursor
from sentry_sdk import capture_event, capture_exception


class AutogestionViewMixin(object):

    def render(self, data):
        return Response(
            json.dumps(data),
            status=200,
            mimetype='application/json'
        )

    def __del__(self):
        self.close_connection()

    def clean_portal(self):
        portal = request.values.get('portal', '')

        return portal.lower()

    def clean_email(self):
        email = request.values.get('email')

        if not email:
            abort(405, description="Invalid email")

        return email.lower()

    def clean_document_type(self):
        document_type = request.values.get('document_type')

        if not document_type:
            abort(405, description="Invalid document_type")

        return document_type.upper()

    def clean_document_number(self):
        document_number = request.values.get('document_number')

        if not document_number:
            abort(405, description="Invalid document_number")

        return document_number.upper()

    def get_cursor(self, connection=None):
        if not connection:
            connection = self.open_connection()

        if isinstance(connection, PostgresConnection):
            cursor = connection.cursor(
                cursor_factory=RealDictCursor
            )

        elif isinstance(connection, MysqlConnection):
            cursor = connection.cursor(DictCursor)

        else:
            abort(500, description="Unknown connection")

        return cursor

    def get_connection(self):
        raise NotImplementedError

    def open_connection(self):
        if not hasattr(self, '_connection'):
            self._connection = self.get_connection()
        return self._connection

    def close_connection(self):
        if getattr(self, '_connection', None):
            self._connection.close()

    def get_paywall_connection(self):
        user = os.getenv('PAYWALL_DB_USER', '')
        password = os.getenv('PAYWALL_DB_PASS', '')
        db_host  = os.getenv('PAYWALL_DB_HOST', '')
        db_port = os.getenv('PAYWALL_DB_PORT', '')
        database = os.getenv('PAYWALL_DB_NAME', '')
        try:
            connection = psycopg2.connect(
                host=db_host,
                port=db_port,
                database=database,
                user=user,
                password=password,
                connect_timeout=3
            )
        except:
            capture_exception()
            abort(500, description="Connection error")
        else:
            return connection

    def get_autogestion_connection(self):
        user = os.getenv('AUTOGESTION_DB_USER', '')
        password = os.getenv('AUTOGESTION_DB_PASS', '')
        db_host  = os.getenv('AUTOGESTION_DB_HOST', '')
        db_port = os.getenv('AUTOGESTION_DB_PORT', '')
        database = os.getenv('AUTOGESTION_DB_NAME', '')
        try:
            connection = psycopg2.connect(
                host=db_host,
                port=db_port,
                database=database,
                user=user,
                password=password,
                connect_timeout=3
            )
        except:
            capture_exception()
            abort(500, description="Connection error")
        else:
            return connection

    def get_peruquiosco_connection(self):
        user = os.getenv('PERUQUIOSCO_DB_USER', '')
        password = os.getenv('PERUQUIOSCO_DB_PASS', '')
        db_host  = os.getenv('PERUQUIOSCO_DB_HOST', '')
        db_port = os.getenv('PERUQUIOSCO_DB_PORT', '')
        database = os.getenv('PERUQUIOSCO_DB_NAME', '')
        try:
            connection = pymysql.connect(
                connect_timeout=3,
                db=database,
                host=db_host,
                passwd=password,
                port=int(db_port),
                user=user,
            )
        except:
            capture_exception()
            abort(500, description="Connection error")
        else:
            return connection

    def get_club_connection(self):
        capture_event(
            {
                'message': 'AutogestionViewMixin.get_club_connection WARNING',
                'extra': {}
            }
        )
        user = os.getenv('CLUB_DB_USER', '')
        password = os.getenv('CLUB_DB_PASS', '')
        db_host  = os.getenv('CLUB_DB_HOST', '')
        db_port = os.getenv('CLUB_DB_PORT', '')
        database = os.getenv('CLUB_DB_NAME', '')
        try:
            connection = pymysql.connect(
                host=db_host,
                user=user,
                passwd=password,
                port=int(db_port),
                db=database,
                connect_timeout=3,
                charset='latin1'
            )
        except:
            capture_exception()
            abort(500, description="Connection error")
        else:
            return connection

    def get_subsonline_connection(self):
        user = os.getenv('SUBSONLINE_DB_USER', '')
        password = os.getenv('SUBSONLINE_DB_PASS', '')
        db_host  = os.getenv('SUBSONLINE_DB_HOST', '')
        db_port = os.getenv('SUBSONLINE_DB_PORT', '')
        database = os.getenv('SUBSONLINE_DB_NAME', '')
        try:
            connection = psycopg2.connect(
                host=db_host,
                port=db_port,
                database=database,
                user=user,
                password=password,
                connect_timeout=3
            )
        except:
            capture_exception()
            abort(500, description="Connection error")
        else:
            return connection


class SiebelBaseMixin(AutogestionViewMixin):

    def get_connection(self):
        return self.get_club_connection()

    def get_subscriber_data(self, document_type=None, document_number=None):
        if document_type is None:
            document_type = self.clean_document_type()

        if document_number is None:
            document_number = self.clean_document_number()

        endpoint = '/wsAutogestionDatos/listado.DatosDetalleSuscriptor'
        payload = {
            'tipoDoc': document_type,
            'nroDoc': document_number,
        }

        result = self.request_siebel(endpoint, payload)

        subscriber = result.get('Suscriptor') or {}
        subscriptions = result.get('DetalleSuscripcion') or []
        return subscriber, subscriptions

    def get_print_subscriptions(self):

        portal = self.clean_portal()
        _, subscriptions = self.get_subscriber_data()

        result = []
        for subscription in subscriptions:

            if (
                self.subscription_is_print(subscription) and
                self.validate_portal(subscription, portal)
            ):
                result.append(subscription)

        return result

    def get_bundle_subscriptions(self):

        portal = self.clean_portal()
        _, subscriptions = self.get_subscriber_data()

        result = []
        for subscription in subscriptions:

            if (
                self.subscription_is_bundle(subscription) and
                self.validate_portal(subscription, portal)
            ):
                result.append(subscription)

        return result

    def subscription_is_print(self, subscription):
        has_print_product = subscription['Tipo'].upper() == 'PRINT'
        has_digital_product = 'DIGITAL' not in subscription['DescPaquete'].upper()
        return has_print_product and has_digital_product

    def subscription_is_bundle(self, subscription):
        has_print_product = subscription['Tipo'].upper() == 'PRINT'
        has_digital_product = 'DIGITAL' in subscription['DescPaquete'].upper()
        return has_print_product and has_digital_product

    def validate_portal(self, subscription, portal):
        subscription_portal = self.get_subscription_portal(subscription)

        return subscription_portal and (
            not portal or subscription_portal == portal
        )

    def get_subscription_portal(self, subscription):

        if subscription['Portal'] == 'GESTION':
            subscription_portal = 'gestion'
        elif subscription['Portal'] == 'EL COMERCIO':
            subscription_portal = 'elcomercio'
        else:
            subscription_portal = ''

        return subscription_portal

    def get_subscription_data(self, subscription_id):
        try:
            subscription_id = int(subscription_id)
        except ValueError:
            abort(400, description="Delivery error")

        endpoint = '/wsAutogestionDatos/listado.DetalleSuscripcion'
        payload = {'codDelivery': subscription_id}

        result = self.request_siebel(endpoint, payload)

        subscription_list = result.get('DetalleSuscripcion')
        return subscription_list[0] if subscription_list else {}

    def get_annulations_data(self, subscription_id):
        endpoint = '/wsAutogestionDatos/listado.SolicitudesAnulacion'
        payload = {'codDelivery': subscription_id}

        result = self.request_siebel(endpoint, payload)

        return result.get('DetalleSolicitudes', [])

    def request_siebel(self, endpoint, payload=None):
        """
            Método para consumir API de siebel
        """
        siebel_url = os.getenv('SIEBEL_API_URL', '')

        url = siebel_url + endpoint

        response = requests.get(url, params=payload)

        if response.status_code == 200:
            result = response.json()

            self.check_result_code(result, response)
        else:
            capture_event(
                {
                    'message': 'request_siebel ERROR %s %s' % (
                        endpoint.split('/')[-1], response.status_code),
                    'extra': {
                        'endpoint': endpoint,
                        'payload': payload,
                        'response': response.text,
                        'url': url,
                    }
                }
            )
            result = {}

        return result

    def check_result_code(self, result, response):
        """
            Verifica el código de respuesta, si es dististinto a 0 o 1 notifica a sentry
        """
        if 'CodRespuesta' in result and result['CodRespuesta'] not in ('0', '1'):
            capture_event(
                {
                    'message': 'Siebel WARNING %s' % result.get('DescRespuesta'),
                    'extra': {
                        'DescRespuesta': result.get('DescRespuesta'),
                        'status_code': response.status_code,
                        'response': response.text,
                        'url': response.url,
                    }
                }
            )


class SiebelBaseView(SiebelBaseMixin, MethodView):
    pass


class AutogestionDetailMixin(AutogestionViewMixin):

    def get_query(self):
        raise NotImplementedError

    def get_query_kwargs(self):
        return {}

    def get_object(self, **kwargs):
        with self.get_cursor() as cursor:
            kwargs.update(
                self.get_query_kwargs()
            )
            cursor.execute(
                self.get_query(),
                kwargs
            )
            obj = cursor.fetchone()

        if not obj:
            abort(404, description="Object not found")

        return obj


class AutogestionDetailView(AutogestionDetailMixin, MethodView):
    pass


class AutogestionListView(AutogestionViewMixin, MethodView):

    def get_query(self):
        raise NotImplementedError

    def get_query_kwargs(self):
        query_kwargs = {
            'document_number': self.clean_document_number(),
            'document_type': self.clean_document_type(),
            'portal': self.clean_portal(),
        }
        return query_kwargs

    def get_objects(self, query=None, query_kwargs=None):
        if not query:
            query = self.get_query()

        if not query_kwargs:
            query_kwargs = self.get_query_kwargs()

        with self.get_cursor() as cursor:
            cursor.execute(
                query,
                query_kwargs
            )
            subscriptions = cursor.fetchall()

        return subscriptions
