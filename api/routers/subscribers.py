"""
    Router que manejan información del suscriptor
"""

from fastapi import APIRouter, HTTPException, Query
from sentry_sdk import capture_event

from ..clients.paywall import PaywallClient
from ..clients.peruquiosco import PeruQuioscoClient
from ..clients.siebel import SiebelClient
from ..models import SubscriberModel

router = APIRouter()


@router.get(
    "/subscriber",
    tags=["Suscriptor"],
    response_model=SubscriberModel)
def subscriber_detail(
        document_type: str = Query(
            ...,
            min_length=3,
            max_length=3,
            description='Tipo de documento'),
        document_number: str = Query(
            ...,
            description='Número de documento')
    ):
    """
        API que retorna los datos del suscriptor.

        Parámetros querystring:
            document_type: Tipo de documento (DNI, RUC, CEX, CID)
            document_number: Número de documento
    """

    # Convierte el tipo de documento a mayúsculas
    document_type = document_type.upper()

    # Descarga los datos del suscriptor
    client = SiebelClient()
    subscriber_data, _ = client.get_subscriber_data(
        document_type=document_type,
        document_number=document_number
    )

    # Si no se encontró suscriptor
    if not subscriber_data and False:
        # Valida que no exista en peruquiosco
        peruquiosco = PeruQuioscoClient()
        subscriber = peruquiosco.get_peruquiosco_subscriber(
            document_type=document_type,
            document_number=document_number)
        if subscriber:
            capture_event(
                {
                    'message': 'PeruQuioscoClient ERROR',
                    'extra': {
                        'documentType': document_type,
                        'documentNumber': document_number,
                        'peruquioscoSubscriber': subscriber
                    }
                }
            )
        # Retorna error 404
        raise HTTPException(
            status_code=404,
            detail="Suscriptor no encontrado")

    # Se crea el objeto para la respuesta
    subscriber = SubscriberModel()
    subscriber.populate(subscriber_data)

    # Si el suscriptor no tiene celular
    if not subscriber.secondPhone:
        # Busca el celular en paywall
        # XXX Paywall debe enviar el celular a Siebel al registrar el cliente
        paywall = PaywallClient()
        subscriber.secondPhone = paywall.get_paywall_phone(
            document_type=document_type,
            document_number=document_number)

    return subscriber
