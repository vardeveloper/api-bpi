from typing import List

import requests
from fastapi import APIRouter, Query

router = APIRouter()


@router.get(
    "/coverage/verification",
    tags=["Mapcity"])
def mapcity_repayments(
        y: str = Query(
            ...,
            description='Latitud'),
        x: str = Query(
            ...,
            description='Longitud'),
        codpro: str = Query(
            ...,
            description='Código de portal')
    ):
    """
        y: latitud
        x: longitud
        codpro: Portal E: elcomercio y T: gestion
        https://amsws.mapcity.pe/index.php/mapdrive/consultarCuadrantes?x=-77.0915966&y=-12.0831063&codpro=T
    """
    mapcity_url = 'https://amsws.mapcity.pe/index.php/mapdrive/consultarCuadrantes'
    mapcity_api_key = 's56ai0v4pzzx4f16ftakjlo97wu7idby'

    payload = {
        'x': x,
        'y': y,
        'codpro': codpro,
        'api': mapcity_api_key,
    }

    response = requests.get(mapcity_url, payload)
    if response.status_code == 200:
        result = response.json()
    else:
        # XXX Notificar errores a sentry y retornar error 500
        result = {}

    return result
