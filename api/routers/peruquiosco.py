"""
    Router que manejan información del suscriptor
"""
from typing import List

from fastapi import APIRouter, HTTPException, Query
from sentry_sdk import capture_event

from ..clients.peruid import PeruidClient
from ..clients.peruquiosco import PeruQuioscoClient
from ..models import ActivateModel, PeruidUserModel

router = APIRouter()


@router.get(
    "/peruquiosco/account/activate",
    tags=["PeruQuiosco"],
    response_model=ActivateModel)
def peruquiosco_activate(email: str):

    client = PeruQuioscoClient()
    data = client.activate_email(email=email)

    result = ActivateModel()
    result.populate(data)

    return result


@router.get(
    "/peruquiosco/peruid/user",
    tags=["PeruQuiosco"],
    response_model=PeruidUserModel)
def peruquiosco_peruid_user(email: str):

    client = PeruQuioscoClient()
    data = client.get_user_by_email(email=email)

    result = PeruidUserModel()
    if ecoid := data.get('us_peruid_ecoid'):

        peruid = PeruidClient()
        if peruid_user := peruid.get_user_by_ecoid(ecoid):
            result.populate(peruid_user)

        else:
            raise HTTPException(status_code=500, detail="Error 500")

    return result
