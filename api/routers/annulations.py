"""
    Router de APIs que retornan datos de anulaciones
"""
from typing import List

from fastapi import APIRouter
from sentry_sdk import capture_event

from ..clients.paywall import PaywallClient
from ..clients.peruquiosco import PeruQuioscoClient
from ..clients.siebel import SiebelClient
from ..clients.subsonline import SubsOnlineClient
from ..models.annulations import (AnnulationBundleModel,
                                  AnnulationDigitalModel,
                                  AnnulationPeruquioscoModel,
                                  AnnulationPrintModel)

router = APIRouter()


@router.get(
    "/subscription-peruquiosco/{subscription_id}/annulations",
    tags=["Anulaciones"],
    response_model=List[AnnulationPeruquioscoModel])
def subscription_peruquiosco_annulations(subscription_id: str):
    client = PeruQuioscoClient()
    subscription_data = client.get_subscription_data(
        subscription_id=subscription_id)
    last_payment_data = client.get_last_payment_data(
        subscription_data=subscription_data)

    result = []
    if subscription_data:
        annulation = AnnulationPeruquioscoModel(
            subscriptionDelivery=last_payment_data.get('delivery')
        )

        if subscription_data.get('sus_activo') == 'Terminado':
            annulation.annulationStatus = 'Cerrado'
            annulation.populate(subscription_data=subscription_data)
            result.append(annulation)

        elif last_payment_data.get('ope_estado') == 2:  # ANULADO
            annulation.annulationStatus = 'Abierto'
            annulation.annulationDate = last_payment_data.get('pope_fec_caducidad')
            result.append(annulation)
            capture_event(
                {
                    'message': 'ClientBase.subscription_peruquiosco_annulations DEBUG',
                    'extra': {
                        'subscription_data': subscription_data,
                        'last_payment_data': last_payment_data,
                    }
                }
            )

    return result


@router.get(
    "/subscription-print/{subscription_id}/annulations",
    tags=["Anulaciones"],
    response_model=List[AnnulationPrintModel])
def subscription_print_annulations(subscription_id: str):
    client = SiebelClient()
    annulations = client.get_annulations(delivery=subscription_id)

    result = []
    for annulation_data in annulations:
        annulation = AnnulationPrintModel()
        annulation.populate(data=annulation_data)
        result.append(annulation)

    return result


@router.get(
    "/subscription-digital/{subscription_id}/annulations",
    tags=["Anulaciones"],
    response_model=List[AnnulationDigitalModel])
def subscription_digital_annulations(subscription_id: str):
    client = PaywallClient()
    subscription_data = client.get_subscription_data(subscription_id=subscription_id)
    delivery = client.get_delivery(subscription_id=subscription_id)

    result = []
    if subscription_data:
        annulation = AnnulationDigitalModel(subscriptionDelivery=delivery)
        annulation.populate(subscription_data=subscription_data)
        if annulation.subscriptionStatus in ('Cancelado', 'Terminado'):
            result.append(annulation)

    return result


@router.get(
    "/subscription-bundle/{subscription_id}/annulations",
    tags=["Anulaciones"],
    response_model=List[AnnulationBundleModel])
def subscription_bundle_annulations(subscription_id: str):
    subscription_data = SubsOnlineClient(
        ).get_subscription_data(subscription_id)
    delivery = subscription_data.get('delivery')

    client = SiebelClient()
    annulations = client.get_annulations(delivery=delivery)

    result = []
    for annulation_data in annulations:
        annulation = AnnulationBundleModel()
        annulation.populate(data=annulation_data)
        result.append(annulation)

    return result
