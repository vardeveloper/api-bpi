from datetime import datetime
from typing import List

from fastapi import APIRouter, HTTPException, Query

from ..clients.paywall import PaywallClient
from ..clients.siebel import SiebelClient
from ..models import (BillModel, ComplaintModel, ComplaintsModel,
                      PaymentMethodModel, ReceivablesModel,
                      SubscriptionItemModel, SubscriptionModel,
                      SubsDigitalItemModel, SubsDigitalModel)

router = APIRouter()


@router.get(
    "/subscriptions",
    tags=["Suscripciones"],
    response_model=List[SubscriptionItemModel])
def subscription_list(
        document_type: str = Query(
            ...,
            min_length=3,
            max_length=3,
            description='Tipo de documento'),
        document_number: str = Query(
            ...,
            description='Número de documento')
    ):
    """
        Retorna la lista de suscripciones (print, bundle y digital)
        que aún no han terminado.
    """

    # Convierte el tipo de documento a mayúsculas
    document_type = document_type.upper()

    # Descarga las suscripciones de paywall
    paywall = PaywallClient()
    subscriptions = paywall.get_subscriptions_data(
        document_type=document_type,
        document_number=document_number
    )

    digital_data = []
    for subscription_data in subscriptions:
        subscription = SubsDigitalItemModel()
        subscription.populate(subscription_data)

        # No se consideran las suscriptiones terminadas
        if subscription.status != 'Terminado':
            # Busca el delivery de la suscripción
            subscription.delivery = paywall.get_delivery(subscription.id)
            # Agrega la suscripción a la lista
            digital_data.append(subscription)

    siebel = SiebelClient()
    _, subscriptions = siebel.get_subscriber_data(
        document_type=document_type,
        document_number=document_number)

    print_data = []
    for subscription_data in subscriptions:
        # Sólo suscripciones print y bundle
        if subscription_data['Tipo'] != 'PRINT':
            continue

        subscription = SubscriptionItemModel()
        subscription.populate(subscription_data)

        # No se consideran las suscriptiones terminadas
        # Las suscripciones deben tener portal
        if subscription.status != 'Terminado' and subscription.portal:
            # Descarga datos faltantes de la suscripción
            subs_data = siebel.get_subscription_data(subscription.delivery)
            subscription.status = subs_data.get('Estado', '').title()
            subscription.siebelNumber = subs_data.get('NroSiebel', '')
            if not subs_data:
                # XXX Alerta porque no se encontraron datos
                print(subscription)

            # Agrega la suscripción a la lista
            print_data.append(subscription)

    return print_data + digital_data


@router.get(
    "/subscription-print/{subscription_id}/details",
    tags=["Suscripciones"],
    response_model=SubscriptionModel)
def subscription_print_detail(subscription_id:str):

    client = SiebelClient()
    subscription_data = client.get_subscription_data(subscription_id)

    # Si no se encuentra la suscripción
    if not subscription_data:
        # Retorna error 404
        raise HTTPException(
            status_code=404,
            detail="Suscripción no encontrada")

    subscription = SubscriptionModel()
    subscription.populate(subscription_data)

    return subscription


@router.get(
    "/subscription-bundle/{subscription_id}/details",
    tags=["Suscripciones"],
    response_model=SubscriptionModel)
def subscription_bundle_detail(subscription_id:str):
    client = SiebelClient()
    subscription_data = client.get_subscription_data(subscription_id)

    # Si no se encuentra la suscripción
    if not subscription_data:
        # Retorna error 404
        raise HTTPException(
            status_code=404,
            detail="Suscripción no encontrada")

    subscription = SubscriptionModel()
    subscription.populate(subscription_data)

    return subscription


@router.get(
    "/subscription-digital/{subscription_id}/details",
    tags=["Suscripciones"],
    response_model=SubscriptionModel)
def subscription_digital_detail(subscription_id:str):
    """
        Detalle de una suscripción digital
    """
    paywall = PaywallClient()
    subscription_data = paywall.get_subscription_data(subscription_id)

    # Si no se encuentra la suscripción
    if not subscription_data:
        # Retorna error 404
        raise HTTPException(
            status_code=404,
            detail="Suscripción no encontrada")

    subscription = SubsDigitalModel()
    subscription.populate(subscription_data)

    # Busca el delivery de la suscripción
    subscription.delivery = paywall.get_delivery(subscription.id)

    return subscription


@router.get(
    "/subscription/{delivery}/receivables",
    tags=["Reclamos"],
    response_model=ReceivablesModel)
def subscription_receivables(delivery: str):
    """
        Consulta deuda pendiente de una suscripción
    """
    siebel = SiebelClient()

    bills = []
    subscription_data = siebel.get_subscription_data(delivery)

    if subscription_data.get('Estado') == 'BAJA POTENCIAL':
        bills_data = siebel.get_bills_data(delivery)

        today = datetime.now().date()
        for bill_data in bills_data:
            bill = BillModel()
            bill.populate(bill_data)

            if bill.status == 'CANCELADO':
                continue

            if bill.startsDate >= today:
                continue

            bills.append(bill)

    receivables = ReceivablesModel(bills=bills)
    receivables.status = subscription_data.get('Estado')
    return receivables


@router.get(
    "/subscription/{delivery}/complaints",
    tags=["Reclamos"],
    response_model=ComplaintsModel)
def subscription_complaints(delivery: str):
    """
        Consulta reclamos de una suscripción
    """
    siebel = SiebelClient()

    last_thisty_days = 0
    complaints = []
    complaints_data = siebel.get_complaints_data(delivery)
    for complaint_data in complaints_data:
        complaint = ComplaintModel()
        complaint.populate(complaint_data)

        if complaint.is_last_days(days=30):
            last_thisty_days += 1
            complaints.append(complaint)

    result = ComplaintsModel(
        complaints=complaints,
        lastThirtyDays=last_thisty_days
    )
    return result


@router.get(
    "/subscription-print/{subscription_id}/payment_method",
    tags=["Reclamos"],
    response_model=PaymentMethodModel)
def subscription_print_payment_method(subscription_id: str):
    """
        Método de pago de print:
        - Se consulta en siebel
        - Si viene de Subsonline es debito automático
    """
    siebel = SiebelClient()

    subscription_data = siebel.get_subscription_data(
        subscription_id=subscription_id)
    debid_data = siebel.get_auto_debid(subscription_id=subscription_id)

    result = PaymentMethodModel()
    result.load_print_data(subscription_data, debid_data)

    return result


@router.get(
    "/subscription-digital/{subscription_id}/payment_method",
    tags=["Reclamos"],
    response_model=PaymentMethodModel)
def subscription_digital_payment_method(subscription_id: str):
    """
        Método de pago de digital:
        - Es debito automático
    """
    siebel = SiebelClient()

    subscription_data = siebel.get_subscription_data(
        subscription_id=subscription_id)

    result = PaymentMethodModel()
    result.load_digital_data(subscription_data)

    return result


@router.get(
    "/subscription-bundle/{subscription_id}/payment_method",
    tags=["Reclamos"],
    response_model=PaymentMethodModel)
def subscription_bundle_payment_method(subscription_id: str):
    """
        Método de pago de digital:
        - Si es de suscripciones online se consulta en su DB
        - Si es de siebel se consulta el API de siebel
    """
    siebel = SiebelClient()

    subscription_data = siebel.get_subscription_data(
        subscription_id=subscription_id)

    result = PaymentMethodModel()
    result.load_bundle_data(subscription_data)

    return result


@router.get(
    "/subscription-peruquiosco/{subscription_id}/payment_method",
    tags=["Reclamos"],
    response_model=PaymentMethodModel)
def subscription_peruquiosco_payment_method(subscription_id: str):
    """
        Método de pago de Digital:
        - Si es de fuente peruquiosco online se consulta en su DB
        - Si es de siebel se consulta el API de siebel
    """
    siebel = SiebelClient()

    subscription_data = siebel.get_subscription_data(
        subscription_id=subscription_id)

    result = PaymentMethodModel()
    result.load_peruquiosco_data(subscription_data)

    return result
