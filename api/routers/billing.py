"""
    Router que manejan información del suscriptor
"""
from typing import List

from fastapi import APIRouter

from ..clients.paywall import PaywallClient
from ..clients.peruquiosco import PeruQuioscoClient
from ..clients.siebel import SiebelClient
from ..clients.subsonline import SubsOnlineClient
from ..models.billing import BillModel, DigitalBillModel, PeruquioscoBillModel

router = APIRouter()


@router.get(
    "/subscription-print/{subscription_id}/billing",
    tags=["Documentos de pago"],
    response_model=List[BillModel])
def subscription_print_billing(subscription_id: str):
    """
        API que retorna los documentos de pago de una
        suscripción print.
    """
    client = SiebelClient()

    payments = client.get_payments(subscription_id)

    result = []
    for _, payment in enumerate(payments):
        bill = BillModel()
        bill.populate(payment)
        result.append(bill)

    return result


@router.get(
    "/subscription-bundle/{subscription_id}/billing",
    tags=["Documentos de pago"],
    response_model=List[BillModel])
def subscription_bundle_billing(subscription_id: str):
    """
        API que retorna los documentos de pago de una
        suscripción bundle.
    """
    subscription_data = SubsOnlineClient(
        ).get_subscription_data(subscription_id)
    delivery = subscription_data.get('delivery')

    client = SiebelClient()
    payments = client.get_payments(delivery)

    result = []
    for _, payment in enumerate(payments):
        bill = BillModel()
        bill.populate(payment)
        result.append(bill)

    return result


@router.get(
    "/subscription-digital/{subscription_id}/billing",
    tags=["Documentos de pago"],
    response_model=List[DigitalBillModel])
def subscription_digital_billing(subscription_id: str):
    """
        API que retorna los documentos de pago de una
        suscripción digital.
    """
    client = PaywallClient()
    sales_orders = client.get_sales_orders(subscription_id)

    result = []
    for order_data in sales_orders:
        bill = DigitalBillModel()
        bill.populate(order_data)
        result.append(bill)

    return result


@router.get(
    "/subscription-peruquiosco/{subscription_id}/billing",
    tags=["Documentos de pago"],
    response_model=List[PeruquioscoBillModel])
def subscription_peruquiosco_billing(subscription_id: str):
    """
        API que retorna los documentos de pago de una
        suscripción digital.
    """
    client = PeruQuioscoClient()
    subscription_data = client.get_subscription_data(subscription_id)
    payments = client.get_payments(subscription_data)

    result = []
    for payment_data in payments:
        bill = PeruquioscoBillModel()
        bill.populate(payment_data)
        result.append(bill)

    return result
