"""
    Router que manejan información del suscriptor
"""
from datetime import date, datetime, timedelta
from typing import List

from dateutil.relativedelta import relativedelta
from fastapi import APIRouter, Query

from ..clients.siebel import SiebelClient
from ..constants import WEEKDAYS
from ..models import PdeModel
from ..models.siebel import SuspensionDateModel

router = APIRouter()


@router.get(
    "/subscriber/repayments",
    tags=["Devolución de dinero"])
def subscriber_repayments(document_type: str, document_number: str):
    """
        API que retorna los datos del suscriptor.

        Parámetros querystring:

            document_type: Tipo de documento (DNI, RUC, CEX, CID)
            document_number: Número de documento
    """
    client = SiebelClient()

    repayments = client.get_repayments(document_type, document_number)

    result = []
    for index, repayment in enumerate(repayments):
        repayment['ss_delivery'] = str(index + 1000000)

        product = repayment.get('Producto') or ''
        if (
            'COMERCIO' in product or
            'GESTION' in product or
            'QUIOSCODIGITAL' in product
        ):
            result.append(repayment)

    return result


@router.get(
    "/subscription/{delivery}/pde",
    tags=["Reclamos"],
    response_model=List[PdeModel])
def subscription_pde(
        delivery: str,
        days: List[date] = Query(
            ...,
            description="Lista de fechas en formato YY-mm-dd")
    ):
    """
        Valida PDE de una fecha o lista de fechas.
    """

    siebel = SiebelClient()

    data = []
    for day in days:
        pde_data = siebel.get_pde_data(delivery, day)
        pde = PdeModel(date=day)
        pde.populate(pde_data)
        data.append(pde)

    return data


@router.get(
    "/subscription/{delivery}/pde/days",
    tags=["Reclamos"],
    response_model=List[PdeModel])
def subscription_pde_days(
        delivery: str,
        limit: int=7
    ):
    """
        Lista las últimas fechas con pde de una suscripción
    """
    max_limit = 30

    if limit > max_limit:
        limit = max_limit

    siebel = SiebelClient()

    subscription_data = siebel.get_subscription_data(delivery)

    data = []
    if delivery_days := subscription_data.get('DiasReparto'):

        weekdays = [WEEKDAYS[d.strip().lower()] for d in delivery_days.split(',')]
        weekdays.sort()

        if weekdays:

            day = datetime.now().date() - timedelta(days=1)
            limit_day = day - timedelta(days=limit)

            while day > limit_day:
                if day.weekday() in weekdays:
                    pde_data = siebel.get_pde_data(delivery, day)
                    pde = PdeModel(date=day)
                    pde.populate(pde_data)
                    data.append(pde)

                day = day - timedelta(days=1)

    return data


@router.get(
    "/subscription/{delivery}/suspension/date",
    tags=["Cambio de dirección"],
    response_model=SuspensionDateModel)
def subscription_suspension_date(delivery: str):
    """
        Retorna la fecha de suspención.

        LUNES SALE PARA LOS MARTES
        MARTES SALE PARA LOS MIERCOLES
        MIERCOLES SALE PARA LOS JUEVES
        JUEVES SALE PARA VIERNES SABADO Y DOMINGO
        VIERNES SALE PARA LOS LUNES
        Hora de corte 2pm pero se considera las 12 m
    """

    siebel = SiebelClient()
    subscription_data = siebel.get_subscription_data(delivery)

    now = datetime.now()

    if now.weekday() in (0, 1, 2):  # Si se solicita: Lunes, martes, miércoles
        if now.hour >= 12:
            start_date = now + timedelta(days=2)  # Desde mañana
        else:
            start_date = now + timedelta(days=1)  # Desde pasado mañana

    elif now.weekday() == 3:  # Si se solicita: Jueves
        if now.hour >= 12:
            start_date = now + relativedelta(weekday=0)  # Desde próximo lunes
        else:
            start_date = now + timedelta(days=1)  # Desde mañana

    elif now.weekday() == 4:  # Si se solicita: Viernes
        if now.hour >= 12:
            start_date = now + relativedelta(weekday=1)  # Desde próximo martes
        else:
            start_date = now + timedelta(days=0)  # Desde próximo lunes

    elif now.weekday() in (5, 6):  # Si se solicita: Sábado y domingo
        start_date = now + relativedelta(weekday=1)  # Desde próximo martes

    if delivery_days := subscription_data.get('DiasReparto'):

        weekdays = [WEEKDAYS[d.strip().lower()] for d in delivery_days.split(',')]
        weekdays.sort()

        if weekdays:
            pass

    suspension_date = SuspensionDateModel()
    suspension_date.populate(start_date.date())

    return suspension_date
