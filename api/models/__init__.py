from datetime import date, datetime, timedelta
from typing import List, Optional

from pydantic import BaseModel
from utils import date_to_string, is_production, timestamp_to_datetime


class SiebelMixin:

    def extract_portal(self, subscription):
        portal = subscription['Portal']
        if portal in ('GESTION', 'GESTION DIGITAL'):
            portal = 'gestion'
        elif portal in ('EL COMERCIO', 'COMERCIO DIGITAL'):
            portal = 'elcomercio'
        else:
            portal = ''
        return portal

    def extract_category(self, subscription):
        category = subscription['Tipo'].lower()

        product = subscription['DescPaquete'].upper()
        if category == 'print' and 'DIGITAL' in product:
            category = 'bundle'

        return category

    def extract_date(self, data, field):
        """
            Convierte 26/12/2017 a date
        """
        text = data.get(field)
        day = None
        if text:
            day = datetime.strptime(text, '%d/%m/%Y').date()
        return day


class DigitalMixin:

    def get_status(self, subscription):
        """
            Recibe los datos de la suscripción y convierte el estado de entero a string
        """

        code = subscription['status']

        status = ''
        if code == 1:
            status = 'Activo'

        elif code == 2:
            status = 'Terminado'

        elif code == 3:
            status = 'Cancelado'

        elif code == 4:
            status = 'Suspendido'

        return status

    def get_periodicity(self, subscription):
        """
            Recibe los datos de la suscripción y retorna la periodicidad
        """

        plan_data = subscription['plan_data']
        # XXX Manejar casos suscripciones sin plan_data
        frequency = plan_data['rates'][-1]['billingFrequency'] if plan_data else ''

        if frequency == 'Day':
            periodicity = 'Diario'

        elif frequency == 'Month':
            periodicity = 'Mensual'

        elif frequency == 'Year':
            periodicity = 'Anual'

        else:
            periodicity = ''

        return periodicity


class SubscriberModel(BaseModel):
    address: str = ''
    dateOfBirth: str = ''
    district: str = ''
    document_number: str = ''
    document_type: str = ''
    email: str = ''
    first_name: str = ''
    gender: str = ''
    id: str = ''
    last_name: str = ''
    phone: str = ''
    profession: str = ''
    secondPhone: str = ''
    second_last_name: str = ''
    documentType: str = ''
    documentNumber: str = ''

    def populate(self, subscriber):
        self.address = subscriber.get('Direccion') or ''
        self.dateOfBirth = subscriber.get('FechaNacimiento') or ''
        self.district = subscriber.get('Distrito') or ''
        self.document_number = subscriber.get('NroDocumento') or ''
        self.document_type = subscriber.get('TipoDocumento') or ''
        self.email = self.extract_email(subscriber)
        self.first_name = subscriber.get('Nombres') or ''
        self.gender = subscriber.get('Sexo') or ''
        self.id = subscriber.get('CodigoSuscriptor') or ''
        self.last_name = subscriber.get('ApellidoPaterno') or ''
        self.phone = self.extract_phone(subscriber, 'Telefono1')
        self.profession = subscriber.get('Profesion') or ''
        self.secondPhone = self.extract_phone(subscriber, 'Telefono2')
        self.second_last_name = subscriber.get('ApellidoMaterno') or ''
        self.documentType = subscriber.get('TipoDocumento') or ''
        self.documentNumber = subscriber.get('NroDocumento') or ''

    def extract_email(self, subscriber):
        email = subscriber.get('Email') or ''

        if email and not is_production():
            email = '%s@mailinator.com' % subscriber['NroDocumento']
            email = email.lower()

        return email or ''

    def extract_phone(self, subscriber, phone_name):
        phone = subscriber[phone_name] or ''
        phone = phone.replace('.0000000', '')
        return phone


class SubscriptionItemModel(SiebelMixin, BaseModel):

    category: str = ''
    delivery: str = ''
    id: str = ''
    periodicity: str = ''
    portal: str = ''
    product: str = ''
    siebelNumber: str = ''
    status: str = ''

    def populate(self, subscription):
        # self.siebelNumber = ''
        # self.status = ''
        self.category = self.extract_category(subscription)
        self.delivery = subscription['CodDelivery']
        self.id = subscription['CodDelivery']
        self.periodicity = subscription['Periodo'].title()
        self.portal = self.extract_portal(subscription)
        self.product = subscription['DescPaquete']

class SubsDigitalItemModel(DigitalMixin, SubscriptionItemModel):

    def populate(self, subscription):
        # 'delivery' = ''
        self.category = 'digital'
        self.id = str(subscription['subscription_id'])
        self.periodicity = self.get_periodicity(subscription)
        self.portal = subscription['portal']
        self.product = subscription['data']['productName']
        self.status = self.get_status(subscription)


class SubscriptionModel(SiebelMixin, BaseModel):

    address: str = ''
    category: str = ''
    delivery: str = ''
    deliveryDays: str = ''
    deliveryMethod: str = ''
    district: str = ''
    email: str = ''
    id: str = ''
    lastPaymentAmount: str = ''
    lastPaymentDate: str = ''
    name: str = ''
    nextPaymentAmount: str = ''
    nextPaymentDate: str = ''
    periodicity: str = ''
    portal: str = ''
    price: str = ''
    product: str = ''
    siebelNumber: str = ''
    status: str = ''
    documentType: str = ''
    documentNumber: str = ''

    def populate(self, subscription):
        self.address = subscription.get('DirecReparto') or ''
        self.category = self.extract_category(subscription)
        self.delivery = subscription['CodDelivery']
        self.deliveryDays = subscription.get('DiasReparto') or ''
        self.district = subscription.get('DistReparto') or ''
        self.email = ""
        self.id = subscription['CodDelivery']
        self.lastPaymentAmount = str(subscription['MntUltPago'])
        self.lastPaymentDate = subscription['FchUltPago']
        self.name = self.product = subscription['DescPaquete']
        self.nextPaymentAmount = ''  # XXX Dato pendiente en el API de Siebel
        self.nextPaymentDate = subscription['FchProxPago']
        self.periodicity = subscription['Periodo'].title()
        self.portal = self.extract_portal(subscription)
        self.price = str(subscription['PrecioActual'])
        self.siebelNumber = subscription['NroSiebel'] or ''
        self.status = subscription['Estado'].title()
        self.documentType = subscription['TipDocumento']
        self.documentNumber = subscription['NroDocumento']


class SubsDigitalModel(DigitalMixin, SubscriptionModel):

    def populate(self, subscription):

        # "delivery": self.get_delivery(subscription)
        self.category = "digital"
        self.deliveryDays = ""
        self.email = self.get_email(subscription)
        self.id = subscription["subscription_id"]
        self.lastPaymentAmount = self.get_last_payment_amount(subscription)
        self.lastPaymentDate = self.get_last_payment_date(subscription)
        self.name = self.product = self.get_product(subscription)
        self.nextPaymentAmount = self.get_next_payment_amount(subscription)
        self.nextPaymentDate = self.get_next_payment_date(subscription)
        self.periodicity = self.get_periodicity(subscription)
        self.price = self.get_price(subscription)
        self.status = self.get_status(subscription)

    def get_last_payment_amount(self, subscription):
        subscription_data = subscription['data']
        last_order = subscription_data['salesOrders'][-1] if subscription_data else {}
        amount = last_order.get('total', '')
        if isinstance(amount, (float, int)):
            amount = "{:.2f}".format(amount)
        return amount

    def get_last_payment_date(self, subscription):
        subscription_data = subscription['data']
        last_order = subscription_data['salesOrders'][-1] if subscription_data else {}
        timestamp = last_order.get('orderDateUTC', '')
        return date_to_string(timestamp_to_datetime(timestamp))

    def get_next_payment_amount(self, subscription):
        plan_data = subscription['plan_data']
        subscription_data = subscription['data']
        cycle = subscription_data.get('currentRetailCycleIDX')
        # XXX rates cuando no tiene plan_data
        rates = plan_data.get('rates')  # Según priceCode
        return self.calculate_next_payment(cycle, rates) or ''

    def calculate_next_payment(self, cycle, rates):
        """
            Calcula el monto a pagar del mes.
            https://arcpublishing.atlassian.net/servicedesk/customer/portal/2/ACS-16111
        """

        if not cycle or not rates:
            return None

        last_rate = None
        retail_cycle = 0
        for rate in rates:
            retail_cycle += rate['durationCount']
            if retail_cycle > cycle:
                if not last_rate:
                    last_rate = rate
                break
            last_rate = rate  # Debe asignarse al final

        return last_rate['amount']

    def get_next_payment_date(self, subscription):
        subscription_data = subscription['data']
        timestamp = subscription_data.get('nextEventDateUTC', '')
        return date_to_string(timestamp_to_datetime(timestamp))

    def get_price(self, subscription):
        return self.get_next_payment_amount(subscription)

    def get_email(self, subscription):
        """
            Retorna el email de login del usuario
        """
        return subscription['email']

    def get_product(self, subscription):
        return subscription['data']['productName']


class DateListModel(BaseModel):
    delivery: str
    days: List[date]


class PdeModel(BaseModel):
    date: date
    pdeExists: bool = False
    message: str = ''

    def populate(self, pde_data):
        if pde_data.get('DescRespuesta') == 'OK':
            self.pdeExists = 'SI' in pde_data['ObservacionReparto']
            self.message = pde_data['ObservacionReparto']


class BillModel(SiebelMixin, BaseModel):
    amount: float = 0
    status: str = ''
    startsDate: date = None
    endsDate: date = None
    paymentMethod: str = ''
    documentType: str = ''

    def populate(self, bill_data):
        self.amount = bill_data.get('ImporteNeto')
        self.documentType = bill_data.get('TipoDocumento')
        self.endsDate = self.extract_date(bill_data, 'FechaFinal')
        self.paymentMethod = bill_data.get('MetodoPago')
        self.startsDate = self.extract_date(bill_data, 'FechaInicial')
        self.status = bill_data.get('EstadoCancelacion')
        # "Empresa": "EMPRESA EDITORA EL COMERCIO S.A.",
        # "FechaCancelacion": "26/12/2017",
        # "FechaEmision": "21/12/2017"
        # "ImporteBruto": 22.03,
        # "ImporteDescuento": 8.47,
        # "ImporteIGV": 2.44,
        # "ImporteNeto": 16,
        # "ImporteRecargo": 0,
        # "ImporteVenta": 13.56,
        # "NroCorrelativo": "2649610",
        # "NroRenovacion": "7097760",
        # "NroSerie": "022",
        # "Observacion": "",
        # "Situacion": "VIGENTE",
        # "TipoRenovacion": "RENOVACION",


class ReceivablesModel(BaseModel):
    status: str = ''
    amountDue: float = 0
    hasAmountDue: bool = False
    bills: List[BillModel] = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        amount = 0
        for bill in self.bills:
            amount += bill.amount
        self.amountDue = amount

        self.hasAmountDue = bool(self.amountDue)


class ComplaintModel(SiebelMixin, BaseModel):
    status: str = ''
    created: date = None
    reason: str = ''
    subReason: str = ''
    observations: str = ''

    def populate(self, complaint_data):
        self.status = complaint_data.get('EstadoSolicitud') or ''
        self.created = self.extract_date(complaint_data, 'FechaSolicitud')
        self.reason = complaint_data.get('MotivoSolicitud') or ''
        self.subReason = complaint_data.get('SubMotivoSolicitud') or ''
        self.observations = complaint_data.get('Observaciones') or ''
        # "Cliente": "VELA BRAVO, JORGE",
        # "CodigoCliente": "1002007",
        # "CodigoDelivery": "193535",
        # "EstadoDelivery": "INACTIVO",
        # "EstadoSolicitud": "Cerrado",
        # "EtapaSolicitud": "Cierre de la SS"
        # "FechaSolicitud": "03/02/2013",
        # "MotivoSolicitud": "DIARIO DEL DIA - 01",
        # "NumeroDocumento": "08728015",
        # "Observaciones": "K Pya Lurin - Cangrejos-Paulina Aguilar/CRTR ANTIGUA PANAMERICANA CASA A-2 - PLAYA PUNTA NEGRA KM. 49 - PLAYA ESCONDIDA, LURIN-NO RECBIO EDIC DEL 03/02",
        # "PeriodoDelivery": "MENSUAL",
        # "Producto": "EL COMERCIO",
        # "SubMotivoSolicitud": "NO RECIBIÓ Y COMPRÓ - 06",
        # "TipoDocumento": "DNI",
        # "TipoSolicitud": "RECLAMOS DE DISTRIBUCIÓN - 04",

    def is_last_days(self, days):

        limit = datetime.now() - timedelta(days=days)

        return limit.date() <= self.created


class ComplaintsModel(BaseModel):
    lastThirtyDays: int = 0
    complaints: List[ComplaintModel] = []


class PaymentMethodModel(BaseModel):
    paymentType: str = ''  # Cargo automático / Pagoefectivo / Depósito en efectivo
    firstSix: str = ''
    lastFour: str = ''
    cardType: str = ''

    def load_print_data(self, subscription_data, debid_data):
        if debid_data:
            pass


class ActivateModel(BaseModel):
    success: bool = False
    message: str = ''

    def populate(self, data):
        messages = data.get('messages')
        if isinstance(messages, str):
            self.message = messages
        elif isinstance(messages, list):
            self.message = messages[0]

        self.success = data['success']


class PeruidUserModel(BaseModel):
    ecoid: str = ''
    first_name: str = ''
    last_name: str = ''
    username: str = ''
    email: str = ''
    gender: str = ''
    status: str = ''
    avatar: str = ''
    birthday: str = ''
    is_active: bool = False

    def populate(self, data):
        self.ecoid = data['ecoid'] or ''
        self.first_name = data['nombres'] or ''
        self.last_name = data['apellidos'] or ''
        self.username = data['nickname'] or ''
        self.email = data['correo'] or ''
        self.gender = data['genero'] or ''
        self.status = data['estado'] or ''
        if self.status == 1:
            self.status = 'activo'
        elif self.status == 0:
            self.status = 'suspendido'
        self.is_active = data['estado'] == 1
        self.avatar = data['avatar'] or ''
        self.birthday = data['dtnacimiento'] or ''
