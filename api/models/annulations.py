from pydantic import BaseModel
from utils import date_to_string, timestamp_to_datetime


class AnnulationModel(BaseModel):
    subscriptionStatus: str = ''
    annulationDate: str = ''
    annulationStatus: str = ''
    subscriptionID: str = ''
    subscriptionDelivery: str = ''


class AnnulationPrintModel(AnnulationModel):

    def populate(self, data):
        self.subscriptionStatus = data['Estado'].title() or ''
        self.annulationDate = data['FchProgramada'] or ''
        self.annulationStatus = data['EstadoSS'] or ''
        self.subscriptionID = data['CodDelivery'] or ''
        self.subscriptionDelivery = data['CodDelivery'] or ''


class AnnulationPeruquioscoModel(AnnulationModel):

    def populate(self, subscription_data):
        self.subscriptionStatus = subscription_data.get('sus_activo') or ''
        self.annulationDate = subscription_data.get('fecha_caducidad') or ''
        self.subscriptionID = subscription_data.get('us_id') or ''


class AnnulationDigitalModel(AnnulationModel):

    def populate(self, subscription_data):
        self.subscriptionStatus = self.get_subscription_status(subscription_data)
        self.annulationDate = self.get_annulation_date(subscription_data)
        self.annulationStatus = self.get_annulation_status(subscription_data)
        self.subscriptionID = subscription_data['subscription_id'] or ''

    def get_annulation_status(self, subscription):
        status = ''
        data = subscription['data']
        if data['status'] == 3:  # Cancelado
            status = 'Abierto'
        elif data['status'] == 2:  # Terminado
            status = 'Cerrado'
        return status

    def get_subscription_status(self, subscription):
        status = ''
        data = subscription['data']
        if data['status'] == 1:  # Activo
            status = 'Activo'
        elif data['status'] == 2:  # Terminado
            status = 'Terminado'
        elif data['status'] == 3:  # Cancelado
            status = 'Cancelado'
        elif data['status'] == 4:  # Suspendido
            status = 'Suspendido'
        return status

    def get_annulation_date(self, subscription):
        _date = ''
        data = subscription['data']
        if data['status'] == 3:  # Cancelado
            # XXX Validar fecha zona horaria
            _date = date_to_string(
                timestamp_to_datetime(data['nextEventDateUTC'])
            )
        elif data['status'] == 2:  # Terminado
            for event in data['events']:
                if event['eventType'] == 'TERMINATE_SUBSCRIPTION':
                    _date = date_to_string(
                        timestamp_to_datetime(event['eventDateUTC'])
                    )
                    break
        return _date


class AnnulationBundleModel(AnnulationModel):

    def populate(self, data):
        self.subscriptionStatus = data['Estado'] or ''
        self.annulationDate = data['FchProgramada'] or ''
        self.annulationStatus = data['EstadoSS'] or ''
        self.subscriptionID = data['CodDelivery'] or ''
        self.subscriptionDelivery = data['CodDelivery'] or ''
