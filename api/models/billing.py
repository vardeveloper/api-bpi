from pydantic import BaseModel
from sentry_sdk.api import start_span
from utils import date_to_string, timestamp_to_datetime


class BillModel(BaseModel):
    dateOfIssue: str = ''
    payStatus: str = ''
    amount: str = ''
    billNumber: str = ''
    billType: str = ''
    company: str = ''
    correlative: str = ''
    observation: str = ''
    paidDay: str = ''
    paymentMethod: str = ''
    renovationNumber: str = ''
    renovationType: str = ''
    startsDate: str = ''
    endsDate: str = ''
    state: str = ''

    def populate(self, data):
        self.amount = data.get('ImporteNeto') or ''
        self.billNumber = data.get('NroSerie') or ''
        self.billType = data.get('TipoDocumento') or ''
        self.company = data.get('Empresa') or ''
        self.correlative = data.get('NroCorrelativo') or ''
        self.dateOfIssue = data.get('FechaEmision') or ''
        self.endsDate = data.get('FechaFinal') or ''
        self.observation = data.get('Observacion') or ''
        self.paidDay = data.get('FechaCancelacion') or ''
        self.payStatus = data.get('EstadoCancelacion') or ''
        self.paymentMethod = data.get('MetodoPago') or ''
        self.renovationNumber = data.get('NroRenovacion') or ''
        self.renovationType = data.get('TipoRenovacion') or ''
        self.startsDate = data.get('FechaInicial') or ''
        self.state = data.get('Situacion') or ''
        # "ImporteBruto": 29.66,
        # "ImporteRecargo": 0.00,
        # "ImporteDescuento": 4.24,
        # "ImporteVenta": 25.42,
        # "ImporteIGV": 4.58,


class DigitalBillModel(BaseModel):
    amount: str = ''
    paidDay: str = ''
    payStatus: str = ''

    def populate(self, order_data):
        data = order_data['data']
        self.amount = data.get('total')
        self.paidDay = date_to_string(
            timestamp_to_datetime(
                data.get('orderDate')
            )
        )
        self.payStatus = data.get('status')


class PeruquioscoBillModel(BaseModel):
    amount: str = ''
    paidDay: str = ''
    payStatus: str = ''

    def populate(self, payment_data):
        self.amount = payment_data.get('pa_monto')
        self.paidDay = str(payment_data.get('pa_fh_creacion'))
        self.payStatus = payment_data.get('pa_estado')
        # ope_estado
        # const ESTADO_PENDIENTE = 0;
        # const ESTADO_ACEPTADO = 1;
        # const ESTADO_ANULADO = 2;
        # //const ESTADO_ERROR = 3;
        # pa_estado
        # const ESTADO_PENDIENTE = 0;
        # const ESTADO_ACEPTADO = 1; //Pagado
        # const ESTADO_ERROR = 2;
        # const ESTADO_EXTORNO = 3;
