from datetime import date

from pydantic import BaseModel


class SuspensionDateModel(BaseModel):
    from_date: date = None
    from_date_text: str = ''

    def populate(self, day):
        self.from_date = day
        self.from_date_text = str(day)
