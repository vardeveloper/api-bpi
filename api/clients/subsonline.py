from .clientbase import ClientBase


class SubsOnlineClient(ClientBase):

    def get_connection(self):
        return self.get_subsonline_connection()

    def get_subscription_data(self, subscription_id):
        query = '''
            SELECT
                "payment_psuscripcion"."id" as subscription_id,
                "payment_psuscripcion"."estado" as status,
                "payment_psuscripcion"."siebel_delivery" as delivery,
                "payment_psuscripcion"."fecha_cargo" as next_payment_date,
                "pgp_tipo_suscripcion"."tips_nombre" as periodicity,
                "pgp_tipo_suscripcion"."tips_precio" as price,
                "epa_producto"."prod_nombre" as product,
                "epa_producto_detalle"."pdet_dias_semana" as days
            FROM
                "payment_psuscripcion"
                INNER JOIN "pgp_perfil_pago"
                        ON ( "payment_psuscripcion"."perfil_pago_id" =
                                "pgp_perfil_pago"."id" )
                INNER JOIN "sis_partners"
                        ON ( "payment_psuscripcion"."partner_id" = "sis_partners"."id" )
                LEFT OUTER JOIN "payment_plan"
                                ON ( "payment_psuscripcion"."plan_id" =
                                "payment_plan"."id" )
                LEFT OUTER JOIN "pgp_tipo_suscripcion"
                                ON ( "payment_plan"."tips_id" =
                                "pgp_tipo_suscripcion"."id" )
                LEFT OUTER JOIN "epa_producto"
                                ON ( "pgp_tipo_suscripcion"."prod_id" =
                                "epa_producto"."id" )
                LEFT OUTER JOIN "epa_producto_detalle"
                                ON ( "epa_producto_detalle"."prod_id" =
                                "epa_producto"."id" )
            WHERE
                "payment_psuscripcion"."id" = %(subscription_id)s
            LIMIT 1;
        '''
        query_kwargs = {
            'subscription_id': subscription_id
        }

        with self.get_cursor() as cursor:
            cursor.execute(
                query,
                query_kwargs
            )
            subscription = cursor.fetchone()

        return subscription or {}

    def get_last_payment(self, subscription_id):
        query = """
            SELECT
                "pgp_pago"."fecha_pago" as payment_date,
                "pgp_pago"."pa_monto" as payment_amount
            FROM
                "pgp_pago"
            WHERE
                "pgp_pago"."psuscripcion_id" = %(subscription_id)s
            ORDER BY
                "pgp_pago"."id" DESC
            LIMIT 1;
        """
        with self.get_cursor() as cursor:
            cursor.execute(
                query,
                {'subscription_id': subscription_id}
            )
            last_payment = cursor.fetchone()

        return last_payment

    def get_next_payment_amount(self, subscription):
        query = """
            SELECT
                "payment_subscriptionpromotion"."amount",
                "payment_subscriptionpromotion"."state"
            FROM
                "payment_subscriptionpromotion"
            WHERE
                "payment_subscriptionpromotion"."psuscripcion_id" = %(subscription_id)s
            ORDER  BY
                "payment_subscriptionpromotion"."id" DESC
            LIMIT 1;
        """
        query_kwargs = {
            "subscription_id": subscription["subscription_id"]
        }

        with self.get_cursor() as cursor:
            cursor.execute(query, query_kwargs)
            promotion = cursor.fetchone()

        # Si la promoción está activa el precio es el de la promoción
        if promotion and promotion['state']:
            amount = str(promotion['amount'])

        # Caso contrario el precio es el de tips_precio
        else:
            amount = str(subscription['price'])

        return amount
