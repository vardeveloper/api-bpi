import os

import requests
from sentry_sdk import capture_event


class PeruidClient:

    def get_user_by_ecoid(self, ecoid):
        url = self.get_peruid_url('/service/usuarios/datos/format/json')
        payload = {
            'ecoid': ecoid
        }

        response = requests.get(url, params=payload, timeout=10)

        if response.status_code == 200:
            result = response.json()
            if result:
                result = result[0]

        else:
            capture_event(
                {
                    'message': 'PeruidClient.get_user_by_ecoid API ERROR',
                    'extra': {
                        'url': url,
                        'payload': payload,
                    }
                }
            )
            result = {}

        return result

    def get_peruid_url(self, endpoint):
        return os.getenv('PERUID_URL', '') + endpoint
