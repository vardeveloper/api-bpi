import os

import requests
from sentry_sdk import capture_event


class SiebelClient:

    siebel_url = None

    def __init__(self, siebel_url=None):
        if siebel_url:
            self.siebel_url = siebel_url
        else:
            self.siebel_url = os.getenv('SIEBEL_API_URL', '')
        super().__init__()

    def get_annulations(self, delivery):

        endpoint = '/wsAutogestionDatos/listado.SolicitudesAnulacion'
        payload = {
            'codDelivery': delivery,
        }

        result = self.request_siebel(endpoint, payload)

        return result.get('DetalleSolicitudes') or []

    def get_payments(self, delivery):

        endpoint = '/wsAutogestionDatos/listado.ComprobantesPago'
        payload = {
            'codDelivery': delivery,
        }

        result = self.request_siebel(endpoint, payload)

        return result.get('ListadoComprobantesPago') or []

    def get_repayments(self, document_type, document_number):

        endpoint = '/wsAutogestionDatos/listado.SolicitudDevolucionDinero'
        payload = {
            'tipoDoc': document_type,
            'nroDoc': document_number,
        }

        result = self.request_siebel(endpoint, payload)

        return result.get('SolicitudDevolucionDionero') or []

    def get_subscriber_data(self, document_type, document_number):

        endpoint = '/wsAutogestionDatos/listado.DatosDetalleSuscriptor'
        payload = {
            'tipoDoc': document_type,
            'nroDoc': document_number,
        }

        result = self.request_siebel(endpoint, payload)

        subscriber = result.get('Suscriptor') or {}
        subscriptions = result.get('DetalleSuscripcion') or []
        return subscriber, subscriptions

    def get_print_subscriptions(self, document_type, document_number, portal=None):

        _, subscriptions = self.get_subscriber_data(document_type, document_number)

        result = []
        for subscription in subscriptions:

            if (
                self.subscription_is_print(subscription) and
                self.validate_portal(subscription, portal)
            ):
                result.append(subscription)

        return result

    def get_bundle_subscriptions(self, document_type, document_number, portal=None):

        _, subscriptions = self.get_subscriber_data(document_type, document_number)

        result = []
        for subscription in subscriptions:

            if (
                self.subscription_is_bundle(subscription) and
                self.validate_portal(subscription, portal)
            ):
                result.append(subscription)

        return result

    def subscription_is_print(self, subscription):
        has_print_product = subscription['Tipo'].upper() == 'PRINT'
        has_digital_product = 'DIGITAL' not in subscription['DescPaquete'].upper()
        return has_print_product and has_digital_product

    def subscription_is_bundle(self, subscription):
        has_print_product = subscription['Tipo'].upper() == 'PRINT'
        has_digital_product = 'DIGITAL' in subscription['DescPaquete'].upper()
        return has_print_product and has_digital_product

    def validate_portal(self, subscription, portal):
        subscription_portal = self.get_subscription_portal(subscription)

        return subscription_portal and (
            not portal or subscription_portal == portal
        )

    def get_subscription_portal(self, subscription):

        if subscription['Portal'] == 'GESTION':
            subscription_portal = 'gestion'
        elif subscription['Portal'] == 'EL COMERCIO':
            subscription_portal = 'elcomercio'
        else:
            subscription_portal = ''

        return subscription_portal

    def get_subscription_data(self, subscription_id):
        try:
            subscription_id = int(subscription_id)
        except ValueError:
            return {}

        endpoint = '/wsAutogestionDatos/listado.DetalleSuscripcion'
        payload = {'codDelivery': subscription_id}

        result = self.request_siebel(endpoint, payload)

        subscription_list = result.get('DetalleSuscripcion')
        return subscription_list[0] if subscription_list else {}

    def get_auto_debid(self, subscription_id):
        endpoint = '/wsAutogestionDatos/lista.DatosCargoAutomatico'
        payload = {'codDelivery': subscription_id}

        result = self.request_siebel(endpoint, payload)

        return result.get('DatosCargoAutomatico') or {}

    def get_annulations_data(self, subscription_id):
        endpoint = '/wsAutogestionDatos/listado.SolicitudesAnulacion'
        payload = {'codDelivery': subscription_id}

        result = self.request_siebel(endpoint, payload)

        return result.get('DetalleSolicitudes', [])

    def request_siebel(self, endpoint, payload=None):
        """
            Método para consumir API de siebel
        """

        url = self.siebel_url + endpoint

        response = requests.get(url, params=payload, timeout=15)

        if response.status_code == 200:
            result = response.json()

            self.check_result_code(result, response)
        else:
            capture_event(
                {
                    'message': 'request_siebel ERROR %s %s' % (
                        endpoint.split('/')[-1], response.status_code),
                    'extra': {
                        'endpoint': endpoint,
                        'payload': payload,
                        'response': response.text,
                        'url': url,
                    }
                }
            )
            result = {}

        return result

    def check_result_code(self, result, response):
        """
            Verifica el código de respuesta, si es dististinto a 0 o 1 notifica a sentry
        """
        if 'CodRespuesta' in result and result['CodRespuesta'] not in ('0', '1'):
            capture_event(
                {
                    'message': 'Siebel ERROR %s' % result.get('DescRespuesta'),
                    'extra': {
                        'DescRespuesta': result.get('DescRespuesta'),
                        'status_code': response.status_code,
                        'response': response.text,
                        'url': response.url,
                    }
                }
            )

    def get_pde_data(self, delivery, day):
        """
            Formato de fecha: 09/03/2021
        """
        endpoint = '/wsAutogestionDatos/validar.PDE'
        payload = {
            'codDelivery': delivery,
            'fchReparto': day.strftime("%d/%m/%Y")
        }
        result = self.request_siebel(endpoint, payload)
        return result

    def get_bills_data(self, delivery):
        """
            Formato de fecha: 09/03/2021
        """
        endpoint = '/wsAutogestionDatos/listado.ComprobantesPago'
        payload = {'codDelivery': delivery}
        result = self.request_siebel(endpoint, payload)

        return result.get('ListadoComprobantesPago') or []

    def get_complaints_data(self, delivery):
        """
            Descarga reclamos de una suscripción
        """
        endpoint = '/wsAutogestionDatos/listado.ReclamoReiterativo'
        payload = {'codDelivery': delivery}
        result = self.request_siebel(endpoint, payload)

        return result.get('DetalleReclamoReiterativo') or []
