from .clientbase import ClientBase


class PaywallClient(ClientBase):

    def get_connection(self):
        return self.get_paywall_connection()

    def get_subscription_data(self, subscription_id):
        query = '''
            SELECT
                "paywall_subscription"."arc_id" as subscription_id,
                "paywall_subscription"."data" as data,
                "paywall_subscription"."state" as status,
                "paywall_plan"."data" as plan_data,
                "paywall_partner"."partner_code" as portal,
                "arcsubs_arcuser"."email" as email
            FROM   "paywall_subscription"
                LEFT OUTER JOIN "paywall_plan"
                                ON ( "paywall_subscription"."plan_id" =
                                "paywall_plan"."id" )
                LEFT OUTER JOIN "paywall_partner"
                                ON ( "paywall_subscription"."partner_id" =
                                    "paywall_partner"."id" )
                LEFT OUTER JOIN "arcsubs_arcuser"
                                ON ( "paywall_subscription"."arc_user_id" =
                                    "arcsubs_arcuser"."id" )
            WHERE  "paywall_subscription"."arc_id" = %(subscription_id)s
            LIMIT 1;
        '''
        query_kwargs = {
            'subscription_id': subscription_id
        }

        with self.get_cursor() as cursor:
            cursor.execute(
                query,
                query_kwargs
            )
            subscription = cursor.fetchone()

        return subscription


    def get_paywall_phone(self, document_type, document_number):
        query = """
            SELECT *
            FROM paywall_paymentprofile
            WHERE
                prof_doc_type = %(document_type)s AND
                prof_doc_num = %(document_number)s AND
                prof_phone IS NOT NULL AND
                siebel_entecode IS NOT NULL
            LIMIT 1;
        """
        query_kwargs = {
            'document_type': document_type,
            'document_number': document_number,
        }

        with self.get_cursor() as cursor:
            cursor.execute(
                query,
                query_kwargs
            )
            profile = cursor.fetchone()

        phone = profile['prof_phone'] if profile else ''
        return phone or ''

    def get_delivery(self, subscription_id):
        """
            Recibe los datos de la suscripción y retorna su código delivery
        """
        query = '''
            SELECT
                "paywall_operation"."siebel_delivery"
            FROM   "paywall_operation"
                INNER JOIN "paywall_payment"
                        ON ( "paywall_operation"."payment_id" = "paywall_payment"."id" )
                INNER JOIN "paywall_subscription"
                        ON ( "paywall_payment"."subscription_id" =
                                "paywall_subscription"."id" )
            WHERE  ( "paywall_subscription"."arc_id" = %(subscription_id)s
                    AND "paywall_operation"."siebel_delivery" IS NOT NULL )
            LIMIT 1;
        '''
        query_kwargs = {'subscription_id': subscription_id}

        with self.get_cursor() as cursor:
            cursor.execute(query, query_kwargs)
            operation = cursor.fetchone()

        delivery = operation['siebel_delivery'] if operation else ''

        return delivery

    def get_subscriptions_data(self, document_type, document_number, portal=None):

        query = '''
            SELECT
                "paywall_subscription"."arc_id" as subscription_id,
                "paywall_subscription"."state" as status,
                "paywall_subscription"."data" as data,
                "paywall_plan"."data" as plan_data,
                "paywall_partner"."partner_code" as portal
            FROM   "paywall_subscription"
                INNER JOIN "paywall_partner"
                        ON ( "paywall_subscription"."partner_id" =
                            "paywall_partner"."id" )
                INNER JOIN "paywall_paymentprofile"
                        ON ( "paywall_subscription"."payment_profile_id" =
                                "paywall_paymentprofile"."id" )
                LEFT OUTER JOIN "paywall_plan"
                                ON ( "paywall_subscription"."plan_id" =
                                "paywall_plan"."id" )
            WHERE  (
                    LOWER("paywall_paymentprofile"."prof_doc_num") = LOWER(%(document_number)s)
        '''

        if portal:
            query += ''' AND "paywall_partner"."partner_code" = %(portal)s '''

        query += ''' AND "paywall_paymentprofile"."prof_doc_type" = %(document_type)s ) ; '''

        query_kwargs = {
            'document_type': document_type,
            'document_number': document_number
        }

        with self.get_cursor() as cursor:
            cursor.execute(query, query_kwargs)
            subscriptions = cursor.fetchall()

        return subscriptions

    def get_sales_orders(self, subscription_id):
        query = '''
            SELECT "paywall_payment"."id",
                "paywall_payment"."created",
                "paywall_payment"."last_updated",
                "paywall_payment"."status",
                "paywall_payment"."data",
                "paywall_payment"."pa_amount",
                "paywall_payment"."date_payment",
                "paywall_payment"."pa_method",
                "paywall_payment"."arc_order",
                "paywall_payment"."payu_order",
                "paywall_payment"."payu_transaction",
                "paywall_payment"."pa_origin",
                "paywall_payment"."pa_gateway",
                "paywall_payment"."subscription_id",
                "paywall_payment"."payment_profile_id",
                "paywall_payment"."partner_id",
                "paywall_payment"."data_loaded",
                "paywall_payment"."transaction_date",
                "paywall_payment"."refund_type",
                "paywall_payment"."refund_amount",
                "paywall_payment"."refund_date"
            FROM   "paywall_payment"
                INNER JOIN "paywall_subscription"
                        ON ( "paywall_payment"."subscription_id" =
                                "paywall_subscription"."id" )
            WHERE  "paywall_subscription"."arc_id" = %(subscription_id)s;
        '''
        query_kwargs = {
            'subscription_id': subscription_id
        }

        with self.get_cursor() as cursor:
            cursor.execute(
                query,
                query_kwargs
            )
            payments = cursor.fetchall()

        return payments or []
