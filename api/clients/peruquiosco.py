import os
import random
import string

import requests
from sentry_sdk import capture_event

from .clientbase import ClientBase


class PeruQuioscoClient(ClientBase):

    def get_connection(self):
        return self.get_peruquiosco_connection()

    def get_subscription_data(self, subscription_id):
        query = '''
            SELECT
                `epa_suscripcion`.`is_recurrence`,
                `epa_suscripcion`.`sus_id`,
                `epa_suscripcion`.`sus_impreso_paq_actual`,
                `epa_suscripcion`.`sus_modalidad_actual`,
                `epa_suscripcion`.`sus_tipo_paquete`,
                IF(
                    epa_suscripcion.sus_fec_caducidad > NOW(),
                    'Activo',
                    'Terminado'
                    ) AS `sus_activo`,
                IF(
                    sus_fh_edicion IS NULL,
                    DATE_FORMAT(
                        sus_fec_inicio,
                        '%%d/%%m/%%Y %%h:%%i %%p'),
                    DATE_FORMAT(
                        sus_fh_edicion,
                        '%%d/%%m/%%Y %%h:%%i %%p')
                    ) AS `fecha_inicio`,
                IF(
                    YEAR(epa_suscripcion.sus_fec_caducidad) = '9999',
                    'Automático',
                    DATE_FORMAT(
                        epa_suscripcion.sus_fec_caducidad,
                        '%%d/%%m/%%Y')
                    ) AS `fecha_caducidad`,
                `auth_usuario`.`documento_tipo_codigo`,
                `auth_usuario`.`us_cod_sus_impresa`,
                `auth_usuario`.`us_confirmado`,
                `auth_usuario`.`us_docnumero_sus_impresa`,
                `auth_usuario`.`us_email`,
                `auth_usuario`.`us_estado`,
                `auth_usuario`.`us_id`,
                `auth_usuario`.`us_nombres`,
                `auth_usuario`.`us_peruid_ecoid`,
                `epa_modalidad_suscripcion`.`mod_nombre`,
                `epa_producto`.`prod_codigo`,
                `epa_producto`.`prod_id`,
                `epa_producto`.`prod_nombre`
            FROM
                `epa_suscripcion`
                INNER JOIN
                    `epa_producto` ON epa_suscripcion.producto_id = epa_producto.prod_id
                INNER JOIN
                    `epa_modalidad_suscripcion` ON
                        epa_suscripcion.sus_modalidad_actual = epa_modalidad_suscripcion.mod_id
                INNER JOIN
                    `auth_usuario` ON epa_suscripcion.usuario_id = auth_usuario.us_id
                LEFT JOIN
                    `pgp_perfil_pago` ON pgp_perfil_pago.tb_users_id = auth_usuario.us_id
            WHERE
                epa_suscripcion.sus_tipo_paquete = 1 AND
                (epa_producto.prod_suscripcion = 1 OR epa_producto.prod_codigo = 'revistaggratis') AND
                `epa_suscripcion`.`sus_id` = %(subscription_id)s
        '''
        query_kwargs = {
            'subscription_id': subscription_id,
        }

        with self.get_cursor() as cursor:
            cursor.execute(
                query,
                query_kwargs
            )
            subscription = cursor.fetchone()

        return subscription or {}

    def get_last_payment_data(self, subscription_data):
        last_payment_data = self.get_last_payment(subscription_data)

        last_payment_data['payment_amount'] = str(
            last_payment_data.get('pa_monto') or
            last_payment_data.get('ope_precio')
        )

        origin = {}
        delivery = ''
        controller = ''
        if last_payment_data.get('pa_id'):
            if last_payment_data.get('subs_id'):
                controller = 'peruquiosco'
                delivery = last_payment_data.get('siebel_delivery', '')
            else:
                delivery = last_payment_data.get('ope_delivery', '')

            if last_payment_data.get('pa_medio_pago') == 'SISAC':
                controller = 'siebel'

            elif last_payment_data.get('pa_medio_pago') in ('VISA', 'PE'):
                controller = 'peruquiosco'

            if not delivery:
                delivery = self.get_delivery_by_pago_id(
                    last_payment_data.get('pa_id')
                )

            if not controller:
                capture_event(
                    {
                        'message': 'PQ.ConPagoSinController WARNING',
                        'extra': {
                            'subscription': subscription_data,
                            'last_payment': last_payment_data,
                        }
                    }
                )

        else:
            if last_payment_data.get('ope_origen') == 'SONLINE':
                controller = 'subsonline'
                capture_event(
                    {
                        'message': 'PQ.SONLINE DEBUG',
                        'extra': {
                            'subscription': subscription_data,
                            'last_payment': last_payment_data,
                        }
                    }
                )

            elif last_payment_data.get('impreso_delivery'):
                controller = 'print'
                origin = self.get_origin_data(
                    last_payment_data.get('impreso_delivery')
                )

            else:
                controller = ''
                capture_event(
                    {
                        'message': 'PQ.SinPagoSinController WARNING',
                        'extra': {
                            'subscription': subscription_data,
                            'last_payment': last_payment_data,
                        }
                    }
                )

        last_payment_data['origin'] = origin or ''
        last_payment_data['delivery'] = delivery or ''
        last_payment_data['controller'] = controller or ''

        return last_payment_data

    def get_origin_data(self, delivery):
        if not delivery:
            return {}

        autogestion_url = os.getenv('AUTOGESTION_API_URL', '')
        autogestion_token = os.getenv('AUTOGESTION_TOKEN', '')
        url = autogestion_url + '/api/v1/subscription-print/%s/details' % delivery
        headers = {
            'Authorization': 'Bearer %s' % autogestion_token
        }

        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            result = response.json()
        else:
            capture_event(
                {
                    'message': 'PeruQuioscoClient.get_origin_data ERROR',
                    'extra': {
                        'AUTOGESTION_API_URL': autogestion_url,
                        'response': response.text,
                        'url': url,
                    }
                }
            )
            result = {'delivery': delivery}

        return result

    def get_delivery_by_pago_id(self, pago_id):
        query_kwargs = {
            'pago_id': pago_id,
        }
        query = '''
            SELECT
                `epa_operacion`.`ope_id`,
                `epa_operacion`.`ope_precio`,
                `epa_operacion`.`ope_origen`,
                `epa_operacion`.`ope_tis_desc`,
                `pgp_pago_operacion`.`pgo_id`,
                `pgp_pago_operacion`.`pago_id`,
                `pgp_pago_operacion`.`operacion_id`,
                `pgp_pago`.`pa_id`,
                `pgp_pago`.`pa_monto`,
                `pgp_pago`.`pa_medio_pago`,
                `epa_operacion`.`cod_sus_impreso` as impreso_delivery,
                `epa_operacion_ov`.`opeo_codigo_suscripcion` as `ope_delivery`,
                `payment_suscripcion`.`subs_id`,
                `payment_suscripcion`.`siebel_delivery` as `siebel_delivery`
            FROM
                `epa_operacion`
            LEFT JOIN `epa_operacion_ov`
                ON ( `epa_operacion_ov`.`operacion_id` = `epa_operacion`.`ope_id` )
            LEFT JOIN `pgp_pago_operacion`
                ON ( `pgp_pago_operacion`.`operacion_id` = `epa_operacion`.`ope_id` )
            LEFT JOIN `pgp_pago`
                ON ( `pgp_pago`.`pa_id` = `pgp_pago_operacion`.`pago_id` )
            LEFT JOIN `payment_suscripcion`
                ON ( `payment_suscripcion`.`subs_id` = `pgp_pago`.`subs_id` )
            WHERE
                `epa_operacion`.`ope_estado` = 1 AND  -- ESTADO_ACEPTADO o PAGADO = 1
                `pgp_pago`.`pa_id` = %(pago_id)s
        '''

        with self.get_cursor() as cursor:
            cursor.execute(query, query_kwargs)
            payments = cursor.fetchall()

        delivery = ''
        for payment in payments:
            delivery = (
                payment.get('siebel_delivery') or
                payment.get('ope_delivery') or ''
            )
            if delivery:
                break

        return delivery or ''

    def get_last_payment(self, subscription_data):
        if not subscription_data:
            return {}

        query_kwargs = {
            'usuario_id':subscription_data['us_id'],
            'producto_id':subscription_data['prod_id'],
        }
        query = '''
            SELECT
                `epa_operacion`.`ope_id`,
                `epa_operacion`.`cod_sus_impreso` as impreso_delivery,
                `epa_operacion`.`ope_estado`,
                `epa_operacion`.`ope_origen`,
                `epa_operacion`.`ope_precio`,
                `epa_operacion`.`ope_fec_final`,
                `epa_operacion`.`ope_tis_desc`,
                `pgp_pago_operacion`.`pgo_id`,
                `pgp_pago_operacion`.`pago_id`,
                `pgp_pago_operacion`.`operacion_id`,
                `pgp_pago`.`pa_id`,
                `pgp_pago`.`pa_monto`,
                `pgp_pago`.`pa_medio_pago`,
                `epa_operacion_ov`.`opeo_codigo_suscripcion` as `ope_delivery`,
                `payment_suscripcion`.`subs_id`,
                `payment_suscripcion`.`siebel_delivery` as `siebel_delivery`,
                DATE_FORMAT(
                    `ope_fec_inicio`,
                    '%%d/%%m/%%Y %%h:%%i %%p') AS `fecha_inicio`
            FROM
                `epa_operacion`
            LEFT JOIN `epa_operacion_ov`
                ON ( `epa_operacion_ov`.`operacion_id` = `epa_operacion`.`ope_id` )
            LEFT JOIN `pgp_pago_operacion`
                ON ( `pgp_pago_operacion`.`operacion_id` = `epa_operacion`.`ope_id` )
            LEFT JOIN `pgp_pago`
                ON ( `pgp_pago`.`pa_id` = `pgp_pago_operacion`.`pago_id` )
            LEFT JOIN `payment_suscripcion`
                ON ( `payment_suscripcion`.`subs_id` = `pgp_pago`.`subs_id` )
            WHERE
                (
                    `epa_operacion`.`ope_estado` = 1 OR  -- ESTADO_ACEPTADO: 1
                    `epa_operacion`.`ope_estado` = 2  -- ESTADO_ANULADO: 2
                ) AND
                `epa_operacion`.`producto_id` = %(producto_id)s AND
                `epa_operacion`.`usuario_id` = %(usuario_id)s
            ORDER  BY
                `epa_operacion`.`ope_id` DESC
            LIMIT 1;
        '''

        with self.get_cursor() as cursor:
            cursor.execute(query, query_kwargs)
            payment = cursor.fetchone()

        return payment or {}

    def get_peruquiosco_subscriber(self, document_type, document_number):
        query = """
            SELECT *
            FROM pgp_perfil_pago
            WHERE
                tipo_doc = %(document_type)s AND
                nro_doc = %(document_number)s AND
                ente_code IS NOT NULL
            LIMIT 1;
        """
        query_kwargs = {
            'document_type': document_type,
            'document_number': document_number,
        }

        with self.get_cursor() as cursor:
            cursor.execute(
                query,
                query_kwargs
            )
            subscriber = cursor.fetchone()

        return subscriber or {}

    def get_user_by_email(self, email):
        query = """
            SELECT *
            FROM
                auth_usuario
            WHERE
                us_email = %(email)s
            LIMIT 1;
        """
        query_kwargs = {
            'email': email,
        }

        with self.get_cursor() as cursor:
            cursor.execute(
                query,
                query_kwargs
            )
            user = cursor.fetchone()

        return user or {}

    def activate_email(self, email):
        endpoint = '/index.php/service/usuarios/rest_activate_undelivered_mail' \
            '/format/json/secretkey/{secret_key}/apikey/{api_key}/correo/{email}'

        secret_key, api_key = self.get_peruid_keys()

        endpoint = endpoint.format(
            secret_key=secret_key,
            api_key=api_key,
            email=email,
        )

        url = self.get_peruid_url(endpoint=endpoint)

        response = requests.get(url)

        if response.status_code == 200:
            return response.json()

    def generate_password(self, length=8):
        letters = string.ascii_lowercase
        result_str = ''.join(random.choice(letters) for i in range(length))
        # result_str = 'pq.2021.' + random.choice(letters)
        return result_str

    def get_peruid_keys(self):
        query = """ SELECT * FROM sys_peruid WHERE pid_host = 'peruquiosco.pe'; """

        with self.get_cursor() as cursor:
            cursor.execute(query)
            obj = cursor.fetchone() or {}

        return obj.get('secret_key'), obj.get('pid_key')

    def get_peruid_url(self, endpoint):
        return os.getenv('PERUID_URL', '') + endpoint

    def get_email_hash(self, email):
        email = email.replace('@', 'arroba')
        endpoint = '/index.php/service/usuarios/rest_solicita_rename_password' \
            '/secretkey/{secret_key}/apikey/{api_key}/correo/{email}/'

        secret_key, api_key = self.get_peruid_keys()

        endpoint = endpoint.format(
            secret_key=secret_key,
            api_key=api_key,
            email=email,
        )

        url = self.get_peruid_url(endpoint=endpoint)

        response = requests.get(url)

        if response.status_code == 200:
            result = response.json()
            return result.get('hash')

    def set_password(self, email, password):
        endpoint = '/index.php/service/usuarios/rest_new_password' \
            '/secretkey/{secret_key}/apikey/{api_key}'\
            '/correo/{email}/hash/{hash}/password/{password}'

        secret_key, api_key = self.get_peruid_keys()

        endpoint = endpoint.format(
            secret_key=secret_key,
            api_key=api_key,
            email=email,
            hash=self.get_email_hash(email),
            password=password,
        )

        url = self.get_peruid_url(endpoint=endpoint)

        response = requests.get(url)
        if response.status_code == 200:
            result = response.json()

        else:
            result = {}
            capture_event(
                {
                    'message': 'PQ.set_password %s %s' % (
                        endpoint, response.status_code),
                    'extra': {
                        'endpoint': endpoint,
                        'response': response.text,
                        'url': url,
                    }
                }
            )

        return result

    def get_payments(self, subscription_data):
        if not subscription_data:
            return {}

        query_kwargs = {
            'usuario_id':subscription_data['us_id'],
            'producto_id':subscription_data['prod_id'],
        }
        query = '''
            SELECT *
            FROM
                epa_operacion
            LEFT JOIN pgp_pago_operacion
                ON ( pgp_pago_operacion.operacion_id = epa_operacion.ope_id )
            LEFT JOIN pgp_pago
                ON ( pgp_pago_operacion.pago_id = pgp_pago.pa_id )
            WHERE  epa_operacion.usuario_id = %(usuario_id)s
                AND epa_operacion.producto_id = %(producto_id)s
            ORDER BY ope_id DESC;
        '''

        with self.get_cursor() as cursor:
            cursor.execute(query, query_kwargs)
            payments = cursor.fetchall()

        return payments or []
