import os

import psycopg2  # https://github.com/jkehler/awslambda-psycopg2
import pymysql
from fastapi import HTTPException
from psycopg2.extensions import connection as PostgresConnection
from psycopg2.extras import RealDictCursor
from pymysql.connections import Connection as MysqlConnection
from pymysql.cursors import DictCursor
from sentry_sdk import capture_event, capture_exception


class ClientBase:

    def __del__(self):
        self.close_connection()

    def get_cursor(self, connection=None):
        if not connection:
            connection = self.open_connection()

        if isinstance(connection, PostgresConnection):
            cursor = connection.cursor(
                cursor_factory=RealDictCursor
            )

        elif isinstance(connection, MysqlConnection):
            cursor = connection.cursor(DictCursor)

        else:
            raise HTTPException(status_code=500, detail="Unknown connection")

        return cursor

    def get_connection(self):
        raise NotImplementedError

    def open_connection(self):
        if not hasattr(self, '_connection'):
            self._connection = self.get_connection()
        return self._connection

    def close_connection(self):
        if getattr(self, '_connection', None):
            self._connection.close()

    def get_paywall_connection(self):
        user = os.getenv('PAYWALL_DB_USER', '')
        password = os.getenv('PAYWALL_DB_PASS', '')
        db_host  = os.getenv('PAYWALL_DB_HOST', '')
        db_port = os.getenv('PAYWALL_DB_PORT', '')
        database = os.getenv('PAYWALL_DB_NAME', '')
        try:
            connection = psycopg2.connect(
                host=db_host,
                port=db_port,
                database=database,
                user=user,
                password=password,
                connect_timeout=3
            )
        except:
            capture_exception()
            raise HTTPException(status_code=500, detail="Connection error")
        else:
            return connection

    def get_autogestion_connection(self):
        user = os.getenv('AUTOGESTION_DB_USER', '')
        password = os.getenv('AUTOGESTION_DB_PASS', '')
        db_host  = os.getenv('AUTOGESTION_DB_HOST', '')
        db_port = os.getenv('AUTOGESTION_DB_PORT', '')
        database = os.getenv('AUTOGESTION_DB_NAME', '')
        try:
            connection = psycopg2.connect(
                host=db_host,
                port=db_port,
                database=database,
                user=user,
                password=password,
                connect_timeout=3
            )
        except:
            capture_exception()
            raise HTTPException(status_code=500, detail="Connection error")
        else:
            return connection

    def get_peruquiosco_connection(self):
        user = os.getenv('PERUQUIOSCO_DB_USER', '')
        password = os.getenv('PERUQUIOSCO_DB_PASS', '')
        db_host  = os.getenv('PERUQUIOSCO_DB_HOST', '')
        db_port = os.getenv('PERUQUIOSCO_DB_PORT', '')
        database = os.getenv('PERUQUIOSCO_DB_NAME', '')
        try:
            connection = pymysql.connect(
                connect_timeout=3,
                db=database,
                host=db_host,
                passwd=password,
                port=int(db_port),
                user=user,
            )
        except:
            capture_exception()
            raise HTTPException(status_code=500, detail="Connection error")
        else:
            return connection

    def get_club_connection(self):
        capture_event(
            {
                'message': 'ClientBase.get_club_connection WARNING',
                'extra': {}
            }
        )
        user = os.getenv('CLUB_DB_USER', '')
        password = os.getenv('CLUB_DB_PASS', '')
        db_host  = os.getenv('CLUB_DB_HOST', '')
        db_port = os.getenv('CLUB_DB_PORT', '')
        database = os.getenv('CLUB_DB_NAME', '')
        try:
            connection = pymysql.connect(
                host=db_host,
                user=user,
                passwd=password,
                port=int(db_port),
                db=database,
                connect_timeout=3,
                charset='latin1'
            )
        except:
            capture_exception()
            raise HTTPException(status_code=500, detail="Connection error")
        else:
            return connection

    def get_subsonline_connection(self):
        user = os.getenv('SUBSONLINE_DB_USER', '')
        password = os.getenv('SUBSONLINE_DB_PASS', '')
        db_host  = os.getenv('SUBSONLINE_DB_HOST', '')
        db_port = os.getenv('SUBSONLINE_DB_PORT', '')
        database = os.getenv('SUBSONLINE_DB_NAME', '')
        try:
            connection = psycopg2.connect(
                host=db_host,
                port=db_port,
                database=database,
                user=user,
                password=password,
                connect_timeout=3
            )
        except:
            capture_exception()
            raise HTTPException(status_code=500, detail="Connection error")
        else:
            return connection
