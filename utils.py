"""
    Módulo con utilitarios
"""

import os
from datetime import datetime, date

from pytz import timezone


def is_production():
    """
        Retorna True si es el ambiente de producción
    """
    return os.getenv('ENVIRONMENT') == 'prod'


def timestamp_to_datetime(timestamp):
    """
        Convierte número entero en datetime
    """
    if isinstance(timestamp, str):
        timestamp = int(timestamp)

    result = None
    if timestamp and isinstance(timestamp, int):
        result = datetime.fromtimestamp(
            timestamp / 1000,
            tz=timezone('America/Lima')
        )

    return result


def json_to_str_converter(obj):
    """
        Conversor de json con soporte para datetime
    """

    if isinstance(obj, (date, datetime)):
        obj = obj.__str__()

    return obj


def date_to_string(data):
    """
        Convierte fechas a string
    """
    # XXX tz to America/Lima

    text = ''
    if data is None:
        pass

    elif isinstance(data, str):
        text = data

    elif isinstance(data, date):
        str_format = '%d/%m/%Y'
        text = data.strftime(str_format)

    elif isinstance(data, datetime):
        str_format = '%d/%m/%Y %H:%M'
        text = data.strftime(str_format)

    return text
