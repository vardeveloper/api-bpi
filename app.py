"""
    Módulo genera url resolver
"""

import os

import sentry_sdk
from dotenv import load_dotenv
from flask import Flask, jsonify
from sentry_sdk.integrations.aws_lambda import AwsLambdaIntegration

from services.automatic_payment import (
    SubscriptionBundleAutomaticPaymentView,
    SubscriptionDigitalAutomaticPaymentView,
    SubscriptionPrintAutomaticPaymentView)
from services.club import SubscriptionClubDetailView
from services.peruquiosco import (PeruQuioscoChangePasswordView,
                                  PeruQuioscoListView)
from services.repayment import (SubscriptionBundleRepaymentListView,
                                SubscriptionDigitalRepaymentListView,
                                SubscriptionPrintRepaymentListView)
from services.subsbundle import (SubscriptionBundleDetailView,
                                 SubscriptionBundleListView)
from services.subscriber import SubscriberDetailView, SubscriberFullDetailView
from services.subsdigital import (SubscriptionDigitalDetailView,
                                  SubscriptionDigitalListView)
from services.subsprint import (SiebelNumberView, SubscriptionPrintDetailView,
                                SubscriptionPrintListView)

# Carga variables de entorno .env
load_dotenv()


# Integración con Sentry
sentry_sdk.init(
    os.getenv('SENTRY_DNS', ''),
    integrations=[AwsLambdaIntegration(), ],
    environment=os.getenv('ENVIRONMENT', '')
)
app = Flask(__name__)


@app.errorhandler(400)
def bad_request(e):
    """
        https://flask.palletsprojects.com/en/1.1.x/patterns/errorpages/
    """
    return jsonify(error=400, message=str(e)), 400


@app.errorhandler(404)
def resource_not_found(e):
    """
        https://flask.palletsprojects.com/en/1.1.x/patterns/errorpages/
    """
    return jsonify(error=404, message=str(e)), 404


@app.errorhandler(405)
def method_not_allowed(e):
    """
        https://flask.palletsprojects.com/en/1.1.x/patterns/errorpages/
    """
    return jsonify(error=405, message=str(e)), 405


@app.errorhandler(500)
def internal_server_error(e):
    """
        https://flask.palletsprojects.com/en/1.1.x/patterns/errorpages/
    """
    return jsonify(error=500, message=str(e)), 500


app.add_url_rule(
    '/api/v1/delivery/<subscription_id>/details',
    view_func=SiebelNumberView.as_view(
        'siebel_number')
)
app.add_url_rule(
    '/api/v1/subscriptor',
    view_func=SubscriberDetailView.as_view(
        'subscriptor_detail')
)
app.add_url_rule(
    '/api/v1/subscriber',
    view_func=SubscriberDetailView.as_view(
        'subscriber_detail')
)
app.add_url_rule(
    '/api/v1/subscriber/full',
    view_func=SubscriberFullDetailView.as_view(
        'subscriber_full_detail')
)
app.add_url_rule(
    '/api/v1/subscriptions/print',
    view_func=SubscriptionPrintListView.as_view(
        'subscription_print_list')
)
app.add_url_rule(
    '/api/v1/subscriptions/bundle',
    view_func=SubscriptionBundleListView.as_view(
        'subscription_bundle_list')
)
app.add_url_rule(
    '/api/v1/subscriptions/digital',
    view_func=SubscriptionDigitalListView.as_view(
        'subscription_digital_list')
)
app.add_url_rule(
    '/api/v1/subscriptions/peruquiosco',
    view_func=PeruQuioscoListView.as_view(
        'subscription_peruquiosco_list')
)
app.add_url_rule(
    '/api/v1/peruquiosco/change-password',
    view_func=PeruQuioscoChangePasswordView.as_view(
        'subscription_peruquiosco_change_password')
)
app.add_url_rule(
    '/api/v1/subscription-print/<subscription_id>/details',
    view_func=SubscriptionPrintDetailView.as_view(
        'subscription_print_detail')
)
app.add_url_rule(
    '/api/v1/subscription-bundle/<subscription_id>/details',
    view_func=SubscriptionBundleDetailView.as_view(
        'subscription_bundle_detail')
)
app.add_url_rule(
    '/api/v1/subscription-digital/<subscription_id>/details',
    view_func=SubscriptionDigitalDetailView.as_view(
        'subscription_digital_detail')
)
app.add_url_rule(
    '/api/v1/subscription-print/<subscription_id>/automatic-payment',
    view_func=SubscriptionPrintAutomaticPaymentView.as_view(
        'subscription_print_payment_method')
)
app.add_url_rule(
    '/api/v1/subscription-bundle/<subscription_id>/automatic-payment',
    view_func=SubscriptionBundleAutomaticPaymentView.as_view(
        'subscription_bundle_payment_method')
)
app.add_url_rule(
    '/api/v1/subscription-digital/<subscription_id>/automatic-payment',
    view_func=SubscriptionDigitalAutomaticPaymentView.as_view(
        'subscription_digital_payment_method')
)
app.add_url_rule(
    '/api/v1/subscription-print/<subscription_id>/repayments',
    view_func=SubscriptionPrintRepaymentListView.as_view(
        'subs_print_repayment_list')
)
app.add_url_rule(
    '/api/v1/subscription-bundle/<subscription_id>/repayments',
    view_func=SubscriptionBundleRepaymentListView.as_view(
        'subs_bundle_repayment_list')
)
app.add_url_rule(
    '/api/v1/subscription-digital/<subscription_id>/repayments',
    view_func=SubscriptionDigitalRepaymentListView.as_view(
        'subs_digital_repayment_list')
)
app.add_url_rule(
    '/api/v1/subscription-club/details',
    view_func=SubscriptionClubDetailView.as_view(
        'subscription_club_detail')
)


# Aplicación
if __name__ == '__main__':
    app.run(debug=os.getenv('ENVIRONMENT') != 'prod')
