"""
    Módulo que controla la autenticación de los lambdas
"""

import os

import sentry_sdk
from sentry_sdk.integrations.aws_lambda import AwsLambdaIntegration

sentry_sdk.init(
    os.getenv('SENTRY_DNS', ''),
    integrations=[AwsLambdaIntegration(), ],
    environment=os.getenv('ENVIRONMENT', '')
)


def auth(event, context):
    """
        Valida que el token del usuario sea correcto
    """

    token = event.get('authorizationToken', '').strip().split()[-1]

    if token == os.getenv('AUTOGESTION_TOKEN', ''):
        policy = {
            'principalId': "rpa3",
            'policyDocument': {
                'Version': '2012-10-17',
                'Statement': [
                    {
                        "Action": "execute-api:Invoke",
                        "Effect": 'Allow',
                        "Resource": '*',  # event['methodArn']
                    }
                ]
            }
        }
        return policy

    raise Exception('Unauthorized')
