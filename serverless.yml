# serverless.yml
service: autogestion-call-service
frameworkVersion: ^2.30.0
exclude:
    - .gitignore
    - .idea
    - .package.json
    - .serverless
    - README.md
    - node_modules**
    - requirements.txt


plugins:
  - serverless-python-requirements
  - serverless-wsgi
  - serverless-plugin-existing-s3
  - serverless-aws-documentation


custom:
  wsgi:
    app: app.app
    packRequirements: false
  pythonRequirements:
    dockerizePip: non-linux
  authorizer:
    users:
      name: authorizerUser
      type: TOKEN
      identitySource: method.request.header.Authorization
      identityValidationExpression: Bearer (.*)
      Properties:
        AuthorizerResultTtlInSeconds: 0


provider:
  name: aws
  runtime: python3.8
  timeout: 30
  stage: ${opt:stage,'test'}
  region: ${file(settings.${opt:stage, self:provider.stage}.yml):AWS_REGION}

  environment:
    AUTOGESTION_API_URL: ${file(settings.${opt:stage, self:provider.stage}.yml):AUTOGESTION_API_URL}
    AUTOGESTION_DB_HOST: ${file(settings.${opt:stage, self:provider.stage}.yml):AUTOGESTION_DB_HOST}
    AUTOGESTION_DB_NAME: ${file(settings.${opt:stage, self:provider.stage}.yml):AUTOGESTION_DB_NAME}
    AUTOGESTION_DB_PASS: ${file(settings.${opt:stage, self:provider.stage}.yml):AUTOGESTION_DB_PASS}
    AUTOGESTION_DB_PORT: ${file(settings.${opt:stage, self:provider.stage}.yml):AUTOGESTION_DB_PORT}
    AUTOGESTION_DB_USER: ${file(settings.${opt:stage, self:provider.stage}.yml):AUTOGESTION_DB_USER}
    AUTOGESTION_TOKEN: ${file(settings.${opt:stage, self:provider.stage}.yml):AUTOGESTION_TOKEN}
    CLUB_DB_HOST: ${file(settings.${opt:stage, self:provider.stage}.yml):CLUB_DB_HOST}
    CLUB_DB_NAME: ${file(settings.${opt:stage, self:provider.stage}.yml):CLUB_DB_NAME}
    CLUB_DB_PASS: ${file(settings.${opt:stage, self:provider.stage}.yml):CLUB_DB_PASS}
    CLUB_DB_PORT: ${file(settings.${opt:stage, self:provider.stage}.yml):CLUB_DB_PORT}
    CLUB_DB_USER: ${file(settings.${opt:stage, self:provider.stage}.yml):CLUB_DB_USER}
    ENVIRONMENT: ${opt:stage,'test'}
    LOG_LEVEL: ${file(settings.${opt:stage, self:provider.stage}.yml):LOG_LEVEL}
    PAYWALL_DB_HOST: ${file(settings.${opt:stage, self:provider.stage}.yml):PAYWALL_DB_HOST}
    PAYWALL_DB_NAME: ${file(settings.${opt:stage, self:provider.stage}.yml):PAYWALL_DB_NAME}
    PAYWALL_DB_PASS: ${file(settings.${opt:stage, self:provider.stage}.yml):PAYWALL_DB_PASS}
    PAYWALL_DB_PORT: ${file(settings.${opt:stage, self:provider.stage}.yml):PAYWALL_DB_PORT}
    PAYWALL_DB_USER: ${file(settings.${opt:stage, self:provider.stage}.yml):PAYWALL_DB_USER}
    PERUID_URL: ${file(settings.${opt:stage, self:provider.stage}.yml):PERUID_URL}
    PERUQUIOSCO_DB_HOST: ${file(settings.${opt:stage, self:provider.stage}.yml):PERUQUIOSCO_DB_HOST}
    PERUQUIOSCO_DB_NAME: ${file(settings.${opt:stage, self:provider.stage}.yml):PERUQUIOSCO_DB_NAME}
    PERUQUIOSCO_DB_PASS: ${file(settings.${opt:stage, self:provider.stage}.yml):PERUQUIOSCO_DB_PASS}
    PERUQUIOSCO_DB_PORT: ${file(settings.${opt:stage, self:provider.stage}.yml):PERUQUIOSCO_DB_PORT}
    PERUQUIOSCO_DB_USER: ${file(settings.${opt:stage, self:provider.stage}.yml):PERUQUIOSCO_DB_USER}
    SENTRY_DNS : ${file(settings.${opt:stage, self:provider.stage}.yml):SENTRY_DNS}
    SIEBEL_API_URL: ${file(settings.${opt:stage, self:provider.stage}.yml):SIEBEL_API_URL}
    SIEBEl_COMISIONES_URL: ${file(settings.${opt:stage, self:provider.stage}.yml):SIEBEl_COMISIONES_URL}
    SUBSONLINE_DB_HOST: ${file(settings.${opt:stage, self:provider.stage}.yml):SUBSONLINE_DB_HOST}
    SUBSONLINE_DB_NAME: ${file(settings.${opt:stage, self:provider.stage}.yml):SUBSONLINE_DB_NAME}
    SUBSONLINE_DB_PASS: ${file(settings.${opt:stage, self:provider.stage}.yml):SUBSONLINE_DB_PASS}
    SUBSONLINE_DB_PORT: ${file(settings.${opt:stage, self:provider.stage}.yml):SUBSONLINE_DB_PORT}
    SUBSONLINE_DB_USER: ${file(settings.${opt:stage, self:provider.stage}.yml):SUBSONLINE_DB_USER}
    VERSION: ${file(settings.${opt:stage, self:provider.stage}.yml):VERSION}

  iamRoleStatements:
   - Effect: Allow
     Action:
       - lambda:InvokeFunction
       - lambda:InvokeAsync
     Resource: "*"


functions:
  authorizerUser:
    handler: authorizer.auth
    cors: true
    description: API de autenticación
    environment:
      VERSION: ${env:VERSION}

  webhook:
    runtime: python3.8
    handler: wsgi.handler
    timeout: 30
    events:
      - http:
          path: /api/v1/delivery/{subscription_id}/details
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscriptor
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscriber
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscriber/full
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscriptions/peruquiosco
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/peruquiosco/change-password
          method: post
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscription-club/details
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscriptions/digital
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscription-digital/{subscription_id}/details
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscription-digital/{subscription_id}/automatic-payment
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscription-digital/{subscription_id}/repayments
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscriptions/bundle
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscription-bundle/{subscription_id}/details
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscription-bundle/{subscription_id}/automatic-payment
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscription-bundle/{subscription_id}/repayments
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscriptions/print
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscription-print/{subscription_id}/details
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscription-print/{subscription_id}/automatic-payment
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1/subscription-print/{subscription_id}/repayments
          method: get
          authorizer: ${self:custom.authorizer.users}
    description: APIs de autogestión de callcenter v1.0
    vpc:
      securityGroupIds: !Split
        - ','
        - ${file(settings.${opt:stage, self:provider.stage}.yml):PROJECT_SECURITY_GROUP_IDS}
      subnetIds: !Split
        - ','
        - ${file(settings.${opt:stage, self:provider.stage}.yml):PROJECT_SUBNET_IDS}
    reservedConcurrency: 100
    memorySize: 128
    tags:
      Environment: ${opt:stage,'test'}
      Stack: autogestion
      Role: service

  app:
    runtime: python3.8
    handler: main.handler
    timeout: 30
    events:
      - http:
          path: /api/v1.1/subscriber
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscriptions
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-print/{subscription_id}/details
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-bundle/{subscription_id}/details
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-digital/{subscription_id}/details
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription/{delivery}/pde
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription/{delivery}/pde/days
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription/{delivery}/suspension/date
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription/{delivery}/receivables
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription/{delivery}/complaints
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscriber/repayments
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/peruquiosco/account/activate
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/peruquiosco/peruid/user
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-print/{subscription_id}/billing
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-bundle/{subscription_id}/billing
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-digital/{subscription_id}/billing
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-peruquiosco/{subscription_id}/billing
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-print/{subscription_id}/annulations
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-bundle/{subscription_id}/annulations
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-digital/{subscription_id}/annulations
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-peruquiosco/{subscription_id}/annulations
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-print/{subscription_id}/payment_method
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-bundle/{subscription_id}/payment_method
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-digital/{subscription_id}/payment_method
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/subscription-peruquiosco/{subscription_id}/payment_method
          method: get
          authorizer: ${self:custom.authorizer.users}
      - http:
          path: /api/v1.1/coverage/verification
          method: get
          authorizer: ${self:custom.authorizer.users}
    description: APIs de autogestión de callcenter v1.1
    vpc:
      securityGroupIds: !Split
        - ','
        - ${file(settings.${opt:stage, self:provider.stage}.yml):PROJECT_SECURITY_GROUP_IDS}
      subnetIds: !Split
        - ','
        - ${file(settings.${opt:stage, self:provider.stage}.yml):PROJECT_SUBNET_IDS}
    reservedConcurrency: 100
    memorySize: 128
    tags:
      Environment: ${opt:stage,'test'}
      Stack: autogestion
      Role: service
